<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePromotionTable extends Migration
{
  public function up()
  {
    Schema::create('promotion', function (Blueprint $table) {
      $table->id();
      $table->string('title_th');
      $table->string('title_en');
      $table->string('title_cn');
      $table->text('description_th')->nullable();
      $table->text('description_en')->nullable();
      $table->text('description_cn')->nullable();
      $table->text('detail_th')->nullable();
      $table->text('detail_en')->nullable();
      $table->text('detail_cn')->nullable();
      $table->date('start_date');
      $table->date('end_date');
      $table->tinyInteger('active')->default(1);
      $table->integer('created_by');
      $table->integer('updated_by');
      $table->timestamps();
      $table->softDeletes('deleted_at', 0);
    });
  }

  public function down()
  {
    Schema::dropIfExists('promotion');
  }
}
