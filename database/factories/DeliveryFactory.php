<?php

use App\Model\Delivery;
use Illuminate\Support\Str;
use Faker\Generator as Faker;


$factory->define(Delivery::class, function (Faker $faker) {
    return [
        'name_th' => $faker->sentence($nbWords = 6, $variableNbWords = true),
        'name_en' => $faker->sentence($nbWords = 6, $variableNbWords = true),
        'name_cn' => $faker->sentence($nbWords = 6, $variableNbWords = true),
        'url' => $faker->url(),
        'created_by' => 1,
        'updated_by' => 1,
        'created_at' => NOW(),
        'updated_at' => NOW(),
    ];
});
