<?php

use App\Model\Promotion;
use Illuminate\Support\Str;
use Faker\Generator as Faker;


$factory->define(Promotion::class, function (Faker $faker) {
    return [
        'title_th' => $faker->sentence($nbWords = 6, $variableNbWords = true),
        'title_en' => $faker->sentence($nbWords = 6, $variableNbWords = true),
        'title_cn' => $faker->sentence($nbWords = 6, $variableNbWords = true),
        'description_th' => $faker->text(),
        'description_en' => $faker->text(),
        'description_cn' => $faker->text(),
        'detail_th' => $faker->text(),
        'detail_en' => $faker->text(),
        'detail_cn' => $faker->text(),
        'start_date' => '2020-12-12 12:12:12',
        'end_date' => '2090-12-12 12:12:12',
        'created_by' => '1',
        'updated_by' => '1',
    ];
});
