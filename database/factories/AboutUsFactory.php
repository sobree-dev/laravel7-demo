<?php

use App\Model\AboutUs;
use Illuminate\Support\Str;
use Faker\Generator as Faker;


$factory->define(AboutUs::class, function (Faker $faker) {
    return [
        'title_th' => $faker->sentence($nbWords = 6, $variableNbWords = true),
        'title_en' => $faker->sentence($nbWords = 6, $variableNbWords = true),
        'title_cn' => $faker->sentence($nbWords = 6, $variableNbWords = true),
        'description1_th' => $faker->text(),
        'description1_en' => $faker->text(),
        'description1_cn' => $faker->text(),
        'description2_th' => $faker->text(),
        'description2_en' => $faker->text(),
        'description2_cn' => $faker->text(),
        'description3_th' => $faker->text(),
        'description3_en' => $faker->text(),
        'description3_cn' => $faker->text(),
        'created_by' => '1',
        'updated_by' => '1',
    ];
});
