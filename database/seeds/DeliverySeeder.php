<?php

use Illuminate\Database\Seeder;
use App\Model\Delivery;

class DeliverySeeder extends Seeder
{
  public function run()
  {

    Delivery::truncate();
    factory(Delivery::class, 10)->create();

  }
}
