<?php

use Illuminate\Database\Seeder;
use App\Model\BuffetType;

class BuffetTypeSeeder extends Seeder
{
  public function run()
  {
    BuffetType::create([
      'name_th' => 'BUFFET 269',
      'name_en' => 'BUFFET 269',
      'name_cn' => 'BUFFET 269',
      'description_th' => 'ไม่รวมเครื่องดื่ม',
      'description_en' => 'Buffet only',
      'description_cn' => 'Buffet only',
      'price' => 269,
      'created_by' => 1,
      'updated_by' => 1,
      'created_at' => NOW(),
      'updated_at' => NOW(),
    ]);

    BuffetType::create([
      'name_th' => 'BUFFET 329',
      'name_en' => 'BUFFET 329',
      'name_cn' => 'BUFFET 329',
      'description_th' => 'รวมน้ำเปล่า และน้ำอัดลม',
      'description_en' => 'Buffet + Soft Drink',
      'description_cn' => 'Buffet + Soft Drink',
      'price' => 329,
      'created_by' => 1,
      'updated_by' => 1,
      'created_at' => NOW(),
      'updated_at' => NOW(),
    ]);

    BuffetType::create([
      'name_th' => 'BUFFET 439',
      'name_en' => 'BUFFET 439',
      'name_cn' => 'BUFFET 439',
      'description_th' => 'รวมน้ำเปล่า น้ำอัดลม และเบียร์',
      'description_en' => 'Buffet + Soft Drink + Beer',
      'description_cn' => 'Buffet + Soft Drink + Beer',
      'price' => 439,
      'created_by' => 1,
      'updated_by' => 1,
      'created_at' => NOW(),
      'updated_at' => NOW(),
    ]);
  }
}
