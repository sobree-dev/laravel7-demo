<?php

use Illuminate\Database\Seeder;
use App\Model\FoodType;

class FoodTypeSeeder extends Seeder
{
  public function run()
  {
    FoodType::create([
      'name_th' => 'Eum eos non harum',
      'name_en' => 'Pork',
      'name_cn' => 'Pork',
      'created_by' => 1,
      'updated_by' => 1,
      'created_at' => NOW(),
      'updated_at' => NOW(),
    ]);

    FoodType::create([
      'name_th' => 'Eum eos non harum',
      'name_en' => 'Meet',
      'name_cn' => 'Meet',
      'created_by' => 1,
      'updated_by' => 1,
      'created_at' => NOW(),
      'updated_at' => NOW(),
    ]);

    FoodType::create([
      'name_th' => 'Eum eos non harum',
      'name_en' => 'Seafood',
      'name_cn' => 'Seafood',
      'created_by' => 1,
      'updated_by' => 1,
      'created_at' => NOW(),
      'updated_at' => NOW(),
    ]);

    FoodType::create([
      'name_th' => 'Eum eos non harum',
      'name_en' => 'Vegetable',
      'name_cn' => 'Vegetable',
      'created_by' => 1,
      'updated_by' => 1,
      'created_at' => NOW(),
      'updated_at' => NOW(),
    ]);
  }
}
