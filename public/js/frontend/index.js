$(document).ready(function() {
		
	sliderRecommend();
	sliderReviews();
	sliderAboutus();
		
});
function sliderRecommend() {
	$(".box-Recommend .owl-carousel").owlCarousel({
		loop:true,
		margin:10,
		autoplay:true,
	    autoplayTimeout:3000,
	    autoplayHoverPause:false,
		nav:false,
		dots:false,
		lazyLoad:true,
		responsiveClass:true,
		responsive:{
			1000:{
				items:3
			},
			700:{
				items:2
			},
			0:{
				items:1
			},
		}
	});
	$('.box-Recommend .owl-carousel .owl-item').on('mouseenter',function(e){
		$(this).closest('.owl-carousel').trigger('stop.owl.autoplay');
	});
	$('.box-Recommend .owl-carousel .owl-item').on('mouseleave',function(e){
		$(this).closest('.owl-carousel').trigger('play.owl.autoplay',[3000]);
	});
	
    $('.box-Recommend .o-prev').click(function() {
        $('.box-Recommend .owl-prev').click();
    });
    $('.box-Recommend .o-next').click(function() {
        $('.box-Recommend .owl-next').click();
    });

}
function sliderReviews() {
	$(".box-reviews .owl-carousel").owlCarousel({
		loop:true,
		margin:20,
		autoplay:true,
	    autoplayTimeout:3000,
	    autoplayHoverPause:false,
		nav:false,
		dots:true,
		lazyLoad:true,
		responsiveClass:true,
		responsive:{
			1000:{
				items:3
			},
			700:{
				items:2
			},
			0:{
				items:1
			},
		}
	});
	$('.box-reviews .owl-carousel .owl-item').on('mouseenter',function(e){
		$(this).closest('.owl-carousel').trigger('stop.owl.autoplay');
	});
	$('.box-reviews .owl-carousel .owl-item').on('mouseleave',function(e){
		$(this).closest('.owl-carousel').trigger('play.owl.autoplay',[3000]);
	});
	
    $('.box-reviews .o-prev').click(function() {
        $('.box-reviews .owl-prev').click();
    });
    $('.box-reviews .o-next').click(function() {
        $('.box-reviews .owl-next').click();
    });

}
function sliderAboutus() {
	$(".box-about-us .owl-carousel").owlCarousel({
		loop:true,
		margin:20,
		autoplay:true,
	    autoplayTimeout:3000,
	    autoplayHoverPause:false,
		nav:false,
		dots:true,
		lazyLoad:true,
		responsiveClass:true,
		responsive:{
			1000:{
				items:3
			},
			700:{
				items:2
			},
			0:{
				items:1
			},
		}
	});
	$('.box-upcoming .owl-carousel .owl-item').on('mouseenter',function(e){
		$(this).closest('.owl-carousel').trigger('stop.owl.autoplay');
	});
	$('.box-upcoming .owl-carousel .owl-item').on('mouseleave',function(e){
		$(this).closest('.owl-carousel').trigger('play.owl.autoplay',[3000]);
	});
}
