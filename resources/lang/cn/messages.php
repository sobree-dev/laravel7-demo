<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'slogan' => 'Et consequatur',
    'price' => '價錢',
    'bath' => '浴',
    'firstname' => '名字',
    'lastname' => '姓',
    'email' => '電子郵件',
    'telephone' => '電話',
    'share' => '分享',
    'home' => '家',
    'tel_short' => '呼叫',
    'office_hours' => '工作時間',
    'branch_contact' => '聯繫',
    'detail' => '詳情',

    // Contact Page
    'input_fullname' => '名稱',
    'input_email' => '電子郵件',
    'input_telephone' => '電話',
    'input_subject' => '學科',
    'input_detail' => '詳情',
    'contact_thankyou' => '感謝您提供信息，我們將在 24 小時內處理。',
    'browse' => '瀏覽',
    'send' => '發送',
    'reset' => '重啟',

];
