<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'slogan' => 'Et consequatur',
    'price' => 'ราคา',
    'bath' => 'บาท',
    'firstname' => 'ชื่อ',
    'lastname' => 'นามสกุล',
    'email' => 'อีเมล',
    'telephone' => 'เบอร์โทรศัพท์',
    'share' => 'แชร์',
    'home' => 'หน้าหลัก',
    'tel_short' => 'โทร.',
    'office_hours' => 'เวลาเปิด-ปิด',
    'branch_contact' => 'ติดต่อสาขา',
    'detail' => 'รายละเอียด',

    // Contact Page
    'input_fullname' => 'ชื่อ',
    'input_email' => 'อีเมล',
    'input_telephone' => 'เบอร์โทร',
    'input_subject' => 'เรื่องที่ต้องการติดต่อ',
    'input_detail' => 'รายละเอียด',
    'contact_thankyou' => 'ขอบคุณสำหรับข้อมูล เจ้าหน้าที่จะดำเนินการให้ภายใน 24 ชม. ค่ะ',
    'browse' => 'แนบไฟล์',
    'send' => 'ส่ง',
    'reset' => 'รีเซ็ต',

];
