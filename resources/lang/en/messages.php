<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'slogan' => 'Et consequatur',
    'price' => 'Price',
    'bath' => 'Bath',
    'firstname' => 'First Name',
    'lastname' => 'Last Name',
    'email' => 'Email',
    'telephone' => 'Telephone',
    'share' => 'Share',
    'home' => 'Home',
    'tel_short' => 'Tel.',
    'office_hours' => 'Open-Close',
    'branch_contact' => 'Contact',
    'detail' => 'Detail',

    // Contact Page
    'input_fullname' => 'Name',
    'input_email' => 'Email',
    'input_telephone' => 'Telephone',
    'input_subject' => 'Subject',
    'input_detail' => 'Detail',
    'contact_thankyou' => 'Thank you for information, we will manage in 24 hr.',
    'browse' => 'Browse',
    'send' => 'Send',
    'reset' => 'Reset',

];
