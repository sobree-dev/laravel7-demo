@extends('frontend.layouts.main')

@section('title')
@endsection

<link href="{{ asset('css/frontend/promotion.css?v') . time() }}" rel="stylesheet" />

@section('content')
  <!-- begin #content -->
  <div id="content" class="content">
    <section class="box-banner BG-bottom-black">
      <h2 class="b-head" data-shadow="Promotion">{{ $pages->{get_lang('title')} }}</h2>
      @foreach($banners as $banner)
        @if($banner->position == 1)
          @foreach($banner->banners_detail as $banner_detail)
            @switch($banner_detail->type)
              @case('image')
                <div class="BG-img" style="background-image: url('{{ $banner_detail->slide_banner_pc }}')"></div>
              @break
            @endswitch
          @endforeach
        @endif
      @endforeach
    </section>
    
    <section class="box-List">
			<div class="container">
				<div class="row">
          @foreach($promotions as $promotion)
            <div class="col-md-4 item" data-type="images">
              <a href="{{ route('frontend.promotion-detail', ['locale' => get_lang(), 'promotion' => $promotion->id]) }}">
                <div class="img">
  								<div class="src-img" style="background-image: url({{ ($promotion->image) ? $promotion->image : 'http://via.placeholder.com/500x400' }})">
                    <img src="{{ asset('images/size-img2.png') }}" alt=""><!-- ช่องนี้ห้ามแก้ -->
                  </div>
                </div>
                <div class="b-profile">
                  <div class="b-name">
                    <h5>{{ $promotion->{get_lang('title')} }}</h5>
                  </div>
                </div>
                <p>{{ $promotion->{get_lang('description')} }}</p>
              </a>
            </div>
          @endforeach
        </div>
      </div>
    </section>

    <section>
      {{ $promotions->withQueryString()->links('frontend.layouts.paginator') }}
    </section>

	</div>
	<!-- end #content -->
@endsection

@push('after-scripts')
@endpush
