@extends('frontend.layouts.main')

@section('title')
@endsection

@push('og')
  <meta property="og:title" content="{{ $promotion->{get_lang('title')} }}">
  <meta property="og:type" content="article">
  <meta property="og:url" content="{{ url()->current() }}">
  <meta property="og:image" content="{{ url($promotion->image) }}">
  <meta property="og:description" content="{!! $promotion->{get_lang('short_description')} !!}">
  <meta name="twitter:card" content="summary">
  <meta name="twitter:site" content="{{ url()->current() }}">
  <meta name="twitter:creator" content="">
  <meta name="twitter:title" content="{{ $promotion->{get_lang('title')} }}">
  <meta name="twitter:image" content="{{ url($promotion->image) }}">
  <meta name="twitter:description" content="{!! $promotion->{get_lang('short_description')} !!}">
@endpush

<link href="{{ asset('css/frontend/detail.css?v') . time() }}" rel="stylesheet" />
<link href="{{ asset('plugins/fancyBox/source/jquery.fancybox.css?v=2.1.5') }}" rel="stylesheet" media="screen" />

<!-- GridGallery -->
<link rel="stylesheet" type="text/css" href="{{ asset('plugins/GridGallery/css/component.css') }}" />

@section('content')
  <!-- begin #content -->
  <div id="content" class="content">
    <section class="box-banner">
      <div class="BG-img" style="background-image: url({{ $promotion->image }});filter: opacity(0.4) blur(3px);"></div>
      <div class="BG-img" style="background-image: url({{ $promotion->image }});background-size: contain;"></div>
   </section>

    <section class="box-detail">
      <div class="container">
        {{-- <div class="text-center pt-4">
          <img src="{{ $promotion->image }}" class="img">
        </div> --}}
        <div class="p p-head">
          <h3>{{ $promotion->{get_lang('title')} }}</h3>
        </div>
        <div class="box-deta">{{ $promotion->created_at->format('d F Y') }} | view {{ number_format($views) }}</div>
        <div class="p">
          {!! $promotion->{get_lang('detail')} !!}
        </div>
      </div>
    </section>
	        			
		<section class="box-social">
			<div class="container">
				<div class="b-social">
					<span>Share : </span>
					<ul class="social">
				    <li>
						  <a class="I-fb" href="{{ get_link_share_facebook() }}" target="_blank" rel="noopener" aria-label="facebook"><img src="{{ asset('images/icon-fb.png') }}"></a>
				    </li>
				    <li>
						  <a class="I-tw" href="{{ get_link_share_twitter( $promotion->{get_lang('title')} ) }}" target="_blank" rel="noopener" aria-label="twitter"><img src="{{ asset('images/icon-tw.png') }}"></a>
				    </li>
					</ul>
				</div>
			</div>
		</section>
  </div>
	<!-- end #content -->
		
@endsection

@push('after-scripts')
<!-- fancybox3 -->
<script src="{{ asset('plugins/fancyBox3/dist/jquery.fancybox.min.js') }}"></script>

<!-- GridGallery -->
<script src="{{ asset('plugins/GridGallery/js/modernizr.custom.js') }}"></script>
<script src="{{ asset('plugins/GridGallery/js/imagesloaded.pkgd.min.js') }}"></script>
<script src="{{ asset('plugins/GridGallery/js/masonry.pkgd.min.js') }}"></script>
<script src="{{ asset('plugins/GridGallery/js/cbpGridGallery.js') }}"></script>

<script>
  $(document).ready(function() {
    new CBPGridGallery( document.getElementById('grid-gallery') );
  });
</script>

@endpush
