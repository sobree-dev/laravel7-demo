@extends('frontend.layouts.main')

@section('title')
@endsection

<link href="{{ asset('css/frontend/menus.css?v') . time() }}" rel="stylesheet" />
<link href="{{ asset('plugins/fancyBox/source/jquery.fancybox.css?v=2.1.5') }}" rel="stylesheet" media="screen" />

@php
  $food_type = '';
@endphp

@section('content')
  <!-- begin #content -->
  <div id="content" class="content">
    <section class="box-banner BG-bottom-black">
      <h2 class="b-head" data-shadow="A la carte">{{ $pages->{get_lang('title')} }}</h2>
      @foreach($banners as $banner)
        @if($banner->position == 1)
          @foreach($banner->banners_detail as $banner_detail)
            @switch($banner_detail->type)
              @case('image')
                <div class="BG-img" style="background-image: url('{{ $banner_detail->slide_banner_pc }}')"></div>
              @break
              @case('video')
                <div class="item">
                  <video src="{{ $banner_detail->banner_video }}" muted autoplay loop></video>
                  <div class="this-none"></div>
                </div>
              @break
              @case('youtube')
                <div class="item">
                  <iframe src="https://www.youtube.com/embed/{{$banner_detail->url}}?mute=1&autoplay=1&loop=1&playlist={{$banner_detail->url}}" frameborder="0"></iframe>
                  <a href="https://www.youtube.com/embed/{{$banner_detail->url}}?autoplay=1&loop=1&playlist={{$banner_detail->url}}" target="_blank"><div class="this-none"></div></a>
                </div>
              @break
            @endswitch
          @endforeach
        @endif
      @endforeach
    </section>
			
    <section class="box-Recommend">
      <div class="container">
        <div class="box-slide">
          <div class="container">
            <div class="owl-carousel">
              @foreach($recommendeds as $recommended)
                @php
                  $diff = Carbon\Carbon::parse($recommended->created_at->format('Y-m-d'))->diffInDays(Carbon\Carbon::now());
                  if ($diff < config('global.day_check_new_item')):
                    $new_item = 'new';
                  else:
                    $new_item = '';
                  endif;
                @endphp
                <div class="item {{ $new_item }}">
                  <a class="fancybox1" href="{{ $recommended->image }}" data-fancybox-group="Recommend" title="&lt;b class='{{ $new_item }}' &gt;&lt;/b&gt;
                  &lt;b&gt; A La cart &lt;/b&gt;
                  &lt;br/&gt; {{ $recommended->{get_lang('name')} }}
                  &lt;br/&gt; {{ __('messages.price') .' '. $recommended->price .' '. __('messages.bath') }}">
                    <div class="img">
                      <div class="src-img" style="background-image: url({{ $recommended->image }})">
                        <img src="{{ asset('images/size-img2.png') }}" alt=""><!-- ช่องนี้ห้ามแก้ -->
                      </div>
                    </div>
                    <h5>A La carte</h5>
                    <p>{{ $recommended->{get_lang('name')} }}</p>
                    <span>{{ __('messages.price') .' '. $recommended->price .' '. __('messages.bath') }}</span>
                  </a>
                </div>
              @endforeach
            </div>
            @if (count($recommendeds) > 3)
              <div class="o-prev"><img src="{{ asset('images/icon-arrow.png') }}"></div>
              <div class="o-next"><img src="{{ asset('images/icon-arrow.png') }}"></div>
            @endif
          </div>
        </div>
      </div>
    </section>
	        
    <section class="box-List">
      <div class="container">
        <div class="row">
          @foreach($alacarte_menus as $alacarte_menu)
            @php
              $diff = Carbon\Carbon::parse($alacarte_menu->created_at->format('Y-m-d'))->diffInDays(Carbon\Carbon::now());
              if ($diff < 7):
                $new_item = 'new';
              else:
                $new_item = '';
              endif;
            @endphp
            @if($alacarte_menu->food_type_id <> $food_type)
              <div class="col-12">
                <h4 class="pt-4">{{ !empty($alacarte_menu->food_type) ? $alacarte_menu->food_type->{get_lang('name')} : '' }}</h4>
              </div>
              @php
                $food_type = $alacarte_menu->food_type_id;
              @endphp
            @endif
            <div class="col-lg-4 col-md-6 py-2">
              <div class="item {{ $new_item }}">
                <a class="fancybox1" href="{{ $alacarte_menu->image }}" data-fancybox-group="menus" title="&lt;b class='new' &gt;&lt;/b&gt;
                  &lt;b&gt; A La cart &lt;/b&gt;
                  &lt;br/&gt; {{ $alacarte_menu->{get_lang('name')} }}
                  &lt;br/&gt; {{ __('messages.price') .' '. $alacarte_menu->price .' '. __('messages.bath') }}">
                  <div class="img">
                    <div class="src-img" style="background-image: url({{ $alacarte_menu->image }})">
                      <img src="{{ asset('images/size-img2.png') }}" alt=""><!-- ช่องนี้ห้ามแก้ -->
					        	</div>
                  </div>
                  <h5>A La cart</h5>
							    <p>{{ $alacarte_menu->{get_lang('name')} }}</p>
                  <span>{{ __('messages.price') .' '. $alacarte_menu->price .' '. __('messages.bath') }}</span>
                </a>
              </div>
					  </div>
          @endforeach
        </div>
        {{-- <h4 class="pt-4">{{ __('messages.detail') }}</h4> --}}
        <div class="row pt-4">
          <div class="col-12">
            {!! ($alacarte->{get_lang('detail')}) ?? '' !!}
          </div>
        </div>
      </div>
    </section>
  </div>
  <!-- end #content -->
		
@endsection

@push('after-scripts')
  <script src="{{ asset('js/frontend/menus.js') }}"></script>
  <script src="{{ asset('plugins/fancyBox/source/jquery.fancybox.js?v=2.1.5') }}"></script>

  <script type="text/javascript">
    $(document).ready(function() {
      $(".fancybox1").fancybox({
        padding: 0 // remove white border
      });
    });
  </script>
@endpush
