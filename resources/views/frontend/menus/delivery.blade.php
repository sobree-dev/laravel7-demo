@extends('frontend.layouts.main')

@section('title')
@endsection

<link href="{{ asset('css/frontend/menus.css?v') . time() }}" rel="stylesheet" />
<link href="{{ asset('plugins/fancyBox/source/jquery.fancybox.css?v=2.1.5') }}" rel="stylesheet" media="screen" />

@section('content')
  <!-- begin #content -->
  <div id="content" class="content">
    <section class="box-banner-text">
      <div class="container">
        <div class="box-text">
					<h2 class="b-head" data-shadow="Take out & Delivery">{{ $pages->{get_lang('title')} }}</h2>
					<div class="quotationMarks" style="margin-left: 10%;">{{ __('messages.slogan') }} ... <t>Demo</t></div>
					<img src="{{ 'http://via.placeholder.com/150x150' }}" class="icon-premium">
        </div>
      </div>
    </section>
						
    <section class="box-tablist">
      <div class="container">
        <ul class="nav" role="tablist">
          @foreach($delivery_menus as $delivery_menu)
            <li role="presentation">
              <a href="#Delivery{{ $delivery_menu->id }}" class="{{ $delivery_menu->id == '1' ? 'active' : '' }}" aria-controls="Delivery{{ $delivery_menu->id }}" role="tab" data-toggle="tab">
                <h3>{{ $delivery_menu->{get_lang('name')} }}</h3>
                <p>{!! $delivery_menu->{get_lang('description')} !!}</p>
              </a>
            </li>
          @endforeach
        </ul>
      </div>
    </section>
	        
    <div class="tab-content">
      @foreach($delivery_menus as $delivery_menu)
        <section class="box-List tab-pane {{ $delivery_menu->id == '1' ? 'active' : '' }}" role="tabpanel" id="Delivery{{ $delivery_menu->id }}">
          <div class="container">
            <div class="row">
              <div class="col-12 py-2">
                <img src="{{ $delivery_menu->image }}">
              </div>
            </div>
            <h4 class="pt-4">{{ __('messages.detail') }}</h4>
            <div class="row">
              <div class="col-12">
                {!! $delivery_menu->{get_lang('detail')} !!}
              </div>
            </div>
          </div>
        </section>
      @endforeach
    </div>
  </div>
  <!-- end #content -->
@endsection

@push('after-scripts')
  <script src="{{ asset('js/frontend/menus.js') }}"></script>
  <script src="{{ asset('plugins/fancyBox/source/jquery.fancybox.js?v=2.1.5') }}"></script>

  <script type="text/javascript">
    $(document).ready(function() {
      $(".fancybox1").fancybox({
        padding: 0 // remove white border
      });
    });
  </script>
@endpush
