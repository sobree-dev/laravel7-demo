<div class="container">
  <ul>
    @foreach($main['menus'] as $key => $menu)
      @if ($menu->pages->id == $pages->parent_id)
        @php $parent_menu_active = 'dropdown active';
        @endphp
      @else
        @php $parent_menu_active = 'dropdown';
        @endphp
      @endif
      @if ($menu->page_id == $pages->id)
        @php $menu_active = 'active';
        @endphp
      @else
        @php $menu_active = '';
        @endphp
      @endif
      <li id="{{ $menu->id }}">
        <a href="{{ collect($menu->childrens)->count() > 0 ? '#' : route($menu->pages->route_name, ['locale' => get_lang()]) }}" class="{{ collect($menu->childrens)->count() > 0 ? 'dropdown-toggle' : '' }}"  data-toggle="{{ collect($menu->childrens)->count() > 0 ? 'dropdown' : '' }}">{{ $menu->{ get_lang('name')} }}</a>
        @if(collect($menu->childrens)->count() > 0)
          <div class="dropdown-menu">
            @foreach ($menu->childrens as $key => $child)
              <a href="{{ collect($child->childrens)->count() > 0 ? '#' : route($child->pages->route_name, ['locale' => get_lang()]) }}" id="{{ $child->id }}" class="dropdown-item">{{ $child->{ get_lang('name') } }}</a>
              </a>
            @endforeach
          </div>
        @endif
      </li>
    @endforeach
  </ul>
  <p class="b-copy">{{ $main['web_info']->{ get_lang('copyright') } }}</p>
</div>
