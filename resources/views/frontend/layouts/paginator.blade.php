@if ($paginator->hasPages())
   <div class="box-pagination">
        <ul>
            {{-- Previous Page Link --}}
            @if ($paginator->onFirstPage())

            @else
                {{-- <li class="">
                    <a class="b-btn arrow-ll" href={{$paginator->url(1) }} ></a>
                </li> --}}
                <li class="">
                    <a class="b-btn arrow-l" href="{{ $paginator->previousPageUrl() }}" ></a>
                </li>
            @endif

            {{-- Pagination Elements --}}
            @foreach ($elements as $element)
                {{-- "Three Dots" Separator --}}

                @if (is_string($element))
                    <li class="page-item disabled" aria-disabled="true"><span class="page-link">{{ $element }}</span></li>
                @endif

                {{-- Array Of Links --}}
                @if (is_array($element))
  
                    @foreach ($element as $page => $url)
                      @if ($paginator->currentPage() > 3 && $page === 2)
                        <li class="page-item disabled"><span class="page-link">...</span></li>
                      @endif
                      @if ($page == $paginator->currentPage())
                        <li class="active"><a href="#">{{ $page }}</a></li>
                      @elseif ($page === $paginator->currentPage() + 1 ||  $page === $paginator->currentPage() - 1 ||  $page === $paginator->lastPage() || $page === 1)
                        <li class=""><a  href="{{ $url }}">{{ $page }}</a></li>  
                      @endif
                      @if ($paginator->currentPage() < $paginator->lastPage() - 2 && $page === $paginator->lastPage() - 1)
                        <li class="page-item disabled"><span class="page-link">...</span></li>
                      @endif
                    @endforeach
                @endif
            @endforeach

            {{-- Next Page Link --}}
            @if ($paginator->hasMorePages())
                <li >
                    <a class="b-btn arrow-r" href="{{ $paginator->nextPageUrl() }}" ></a>
                </li>
                {{-- <li >
                    <a class="b-btn arrow-rr" href="{{ $paginator->url($paginator->lastPage()) }}" ></a>
                </li> --}}
            @else

            @endif
        </ul>
  </div>
@endif
