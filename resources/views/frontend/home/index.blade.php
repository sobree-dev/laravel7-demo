@extends('frontend.layouts.main')

@section('title')
@endsection

<link href="{{ asset('css/frontend/index.css?v') . time() }}" rel="stylesheet" />
<link href="{{ asset('plugins/fancyBox/source/jquery.fancybox.css?v=2.1.5') }}" rel="stylesheet" media="screen" />

@section('content')
	<!-- begin #content -->
	<div id="content" class="content">
    <section class="box-banner BG-bottom">
      <div class="owl-carousel">
        @foreach($banners as $banner)
          @if($banner->position == 1)
            @foreach($banner->banners_detail as $banner_detail)
              @switch($banner_detail->type)
                @case('image')
                  <div class="item">
                    <div class="img-banner banner-portrait" style="background-image: url('{{ $banner_detail->slide_banner_mobile }}')"></div>
                    <div class="img-banner" style="background-image: url('{{ $banner_detail->slide_banner_pc }}')"></div>
                  </div>
                @break
                @case('video')
                  <div class="item">
                    <video src="{{ $banner_detail->banner_video }}" muted autoplay loop></video>
                    <div class="this-none"></div>
                  </div>
                @break
                @case('youtube')
                  <div class="item">
                    <iframe src="https://www.youtube.com/embed/{{$banner_detail->url}}?mute=1&autoplay=1&loop=1&playlist={{$banner_detail->url}}" frameborder="0"></iframe>
                    <a href="https://www.youtube.com/embed/{{$banner_detail->url}}?autoplay=1&loop=1&playlist={{$banner_detail->url}}" target="_blank"><div class="this-none"></div></a>
                  </div>
                @break
              @endswitch
            @endforeach
          @endif
        @endforeach
      </div>
    </section>
    <section class="box-Ourstory">
			<div class="container width90">
				<div class="row">
					<div class="col-md-6">
						<div class="b-text">
							<img src="{{ !empty($main['web_info']->logo_head) ? $main['web_info']->logo_head : 'http://via.placeholder.com/150x150' }}" class="icon-Logo">
							<h2 class="b-head t-color t-left">{{ $aboutus->{get_lang('title')} }}</h2>
							<p>{{ $aboutus->{get_lang('description1')} }} ...</p>
							<a href="{{ route('frontend.about', ['locale' => get_lang()]) }}" class="btn2">View More +</a>
						</div>
					</div>
					<div class="col-md-6">
						<img src="http://via.placeholder.com/500x300" class="img-Ourstory">
						<div class="quotationMarks" style="margin-left: 10%;">Information Security ... <t>Demo</t></div>
						{{-- <img src="{{ asset('images/icon-premium.png') }}" class="icon-premium"> --}}
					</div>
				</div>
	    </div>
    </section>

    <section class="box-Menus">
		  <h2 class="b-head" data-shadow="Menus">Menus</h2>
			<div class="container">
				<div class="row">
					<div class="col-md-4">
						<div class="box-list">
							<img src="{{ ($main['web_info']->image_alacarte) ? $main['web_info']->image_alacarte : 'http://via.placeholder.com/500x500' }}">
							<div class="list">
								<h2 class="b-head">A la cart</h2>
								<a href="{{ route('frontend.alacarte_menu', ['locale' => get_lang()]) }}" class="btn2">View More +</a>
				      </div>
            </div>
						<img src="{{ asset('images/size-img.png') }}" class="size-img"><!-- ช่องนี้ห้ามแก้ -->
		      </div>
					<div class="col-md-4">
						<div class="box-list">
							<img src="{{ ($main['web_info']->image_buffet) ? $main['web_info']->image_buffet : 'http://via.placeholder.com/500x500' }}">
							<div class="list">
								<h2 class="b-head">Buffet</h2>
								<a href="{{ route('frontend.buffet_menu', ['locale' => get_lang()]) }}" class="btn2">View More +</a>
				      </div>
            </div>
						<img src="{{ asset('images/size-img.png') }}" class="size-img"><!-- ช่องนี้ห้ามแก้ -->
		      </div>
          <div class="col-md-4">
            <div class="box-list">
							<img src="{{ ($main['web_info']->image_delivery) ? $main['web_info']->image_delivery : 'http://via.placeholder.com/500x500' }}">
              <div class="list">
                <h2 class="b-head">Delivery</h2>
                <a href="{{ route('frontend.delivery_menu', ['locale' => get_lang()]) }}" class="btn2">View More +</a>
              </div>
            </div>
            <img src="{{ asset('images/size-img.png') }}" class="size-img"><!-- ช่องนี้ห้ามแก้ -->
          </div>
        </div>
      </div>
    </section>

    <section class="box-Recommend hidden-none">
      <h2 class="b-head" data-shadow="Recommend">Recommend</h2>
      <div class="box-slide">
        <div class="container">
          <div class="owl-carousel">
            @php
              $date_now = date('Y-m-d');
            @endphp
            @foreach($recommendeds as $recommended)
              @php
                $diff = Carbon\Carbon::parse($recommended->updated_at->format('Y-m-d'))->diffInDays(Carbon\Carbon::now());
                if ($diff < config('global.day_check_new_item')):
                  $new_item = 'new';
                else:
                  $new_item = '';
                endif;
              @endphp
              <div class="item {{ $new_item }}">
  							<a class="fancybox1" href="{{ $recommended->image }}" data-fancybox-group="gallery" title="&lt;b class='{{ $new_item }}' &gt;&lt;/b&gt;
                  &lt;b&gt; {{ !empty($recommended->price) ? 'A la carte' : 'Buffet' }} &lt;/b&gt;
                  &lt;br/&gt; {{ $recommended->{get_lang('name')} }}
                  &lt;br/&gt; {{ !empty($recommended->price) ? __('messages.price') .' '. $recommended->price .' '. __('messages.bath') : '' }}">
                  <div class="img">
                    <div class="src-img" style="background-image: url({{ $recommended->image }})">
                      <img src="{{ asset('images/size-img2.png') }}" alt=""><!-- ช่องนี้ห้ามแก้ -->
                    </div>
                  </div>
                  @if (!empty($recommended->price))
                    <h5>A La carte</h5>
                    <p>{{ $recommended->{get_lang('name')} }}</p>
                    <span>{{ __('messages.price') .' '. $recommended->price .' '. __('messages.bath') }}</span>
                  @else
                    <h5>Buffet</h5>
                    <p>{{ $recommended->{get_lang('name')} }}</p>
                    <span>&nbsp;</span>
                  @endif
                </a>
              </div>
            @endforeach
          </div>
          @if (count($recommendeds) > 3)
            <div class="o-prev"><img src="{{ asset('images/icon-arrow.png') }}"></div>
            <div class="o-next"><img src="{{ asset('images/icon-arrow.png') }}"></div>
          @endif
        </div>
      </div>
    </section>

    <section class="box-reviews">
      <div class="box-head">
        <div class="container">
          <h2 class="b-head" data-shadow="Reviews">Reviews</h2>
        </div>
      </div>
      @if(!empty($main_review))
        <div class="container">
          <div class="box-video">
            @foreach($main_review as $banner)
              @if($banner->position == 1)
                @foreach($banner->banners_detail as $banner_detail)
                  @switch($banner_detail->type)
                    @case('image')
                      <div class="BG-img" style="background-image: url('{{ $banner_detail->slide_banner_pc }}')"></div>
                    @break
                    @case('youtube')
                      <iframe src="https://www.youtube.com/embed/{{$banner_detail->url}}?mute=1&autoplay=1&loop=1&playlist={{$banner_detail->url}}" frameborder="0"></iframe>
                    @break
                    @case('facebook')
                      <iframe src="https://www.facebook.com/plugins/video.php?autoplay=true&href=https%3A%2F%2Fwww.facebook.com%2FBestBeefSukhumvit%2Fvideos%2F{{ $banner_detail->url }}%2F&show_text=0&width=560" width="560" height="315" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true" allowFullScreen="true"></iframe>
                    @break
                  @endswitch
                @endforeach
              @endif
            @endforeach
          </div>
        </div>
      @endif
      <div class="box-slide">
        <div class="container">
          <div class="owl-carousel">
            @foreach($reviews as $review)
              <div class="item" data-type="image">
                <a href="{{ route('frontend.review-detail', ['locale' => get_lang(), 'review' => $review->id]) }}">
                  <div class="img">
                    <div class="src-img" style="background-image: url({{ ($review->image) ? $review->image : 'http://via.placeholder.com/500x300' }})">
                      <img src="{{ asset('images/size-img2.png') }}" alt=""><!-- ช่องนี้ห้ามแก้ -->
                    </div>
                  </div>
                  <div class="b-profile">
                    <img src="{{ !empty($review->image_user) ? $review->image_user : 'http://via.placeholder.com/150x150' }}">
                    <div class="b-name">
                      <h5>{{ $review->{get_lang('title')} }}</h5>
                      <div class="rateyo" data-rateyo-rating="{{ $review->point * 10 }}%"></div>
                    </div>
                  </div>
                  <p>{{ $review->{get_lang('short_description')} }}</p>
                </a>
              </div>
            @endforeach
          </div>
          @if (count($reviews) > 3)
            <div class="o-prev"><img src="{{ asset('images/icon-arrow.png') }}"></div>
            <div class="o-next"><img src="{{ asset('images/icon-arrow.png') }}"></div>
          @endif
        </div>
      </div>
      @if (count($reviews) > 3)
        <a href="{{ route('frontend.review', ['locale' => get_lang()]) }}" class="btn2">View More +</a>
      @endif
    </section>
	        
    <section class="box-about-us">
      <div class="container">
        <h2 class="b-head t-color">Say About Us!</h2>
        <div class="owl-carousel">
          @foreach($customer_reviews as $customer_review)
            <div class="item">
              <div class="box-profile">
                <img src="{{ !empty($customer_review->image_user) ? $customer_review->image_user : 'http://via.placeholder.com/150x150' }}">
              </div>
              <h4>{{ $customer_review->{get_lang('title')} }}</h4>
  						<div class="rateyo" data-rateyo-rating="{{ $customer_review->point * 10 }}%"></div>
  						<p>{{ $customer_review->{get_lang('short_description')} }}</p>
            </div>
          @endforeach
        </div>
			</div>
    </section>

    <section class="box-banner2">
			<div class="container">
				<div class="row">
          @foreach($deliveries as $delivery)
						<div class="col-4">
							<a href="{{ !empty($delivery->url) ? $delivery->url : '#' }}">
								<img src="{{ ($delivery->image) ? $delivery->image : 'http://via.placeholder.com/500x300'}}" class="img-fluid">
							</a>
						</div>
          @endforeach
				</div>
			</div>
    </section>
	        
    <section class="box-branch">
      <div class="container">
        <div class="row">
          @foreach($branchs as $branch)
            <div class="col-md-4 list">
              <div class="head-off-on">
                <b>Et consequatur accusamus est pariatur hic</b><br>
                <b class="I-map">{{ $branch->{get_lang('name')} }}</b>
              </div>
              <div class="text-off-on">
                <p>{{ $branch->{get_lang('address')} }}</p>
                <p>{{ __('messages.tel_short') }} : {{ $branch->telephone }}</p>
  							<p>{{ __('messages.office_hours') }} : {{ $branch->office_hours }}</p>
                <a href="{{ !empty($branch->line) ? $branch->line : '#' }}" target="_blank" class="btn-line">{{ __('messages.branch_contact') }}</a>
              </div>
            </div>
          @endforeach
        </div>
      </div>
    </section>
  </div>
@endsection

@push('after-scripts')
  <script src="{{ asset('js/frontend/index.js') }}"></script>
  <script src="{{ asset('plugins/fancyBox/source/jquery.fancybox.js?v=2.1.5') }}"></script>

  <script type="text/javascript">
    $(document).ready(function() {
      $(".fancybox1").fancybox({
        padding: 0 // remove white border
      });
    });
  </script>
@endpush
