@extends('frontend.layouts.main')

@section('title')
@endsection

@push('og')
  {{-- <meta property="og:title" content="gallery">
  <meta property="og:type" content="article">
  <meta property="og:url" content="{{ url()->current() }}">
  <meta property="og:image" content="{{ url($gallery->image[0]->getUrl()) }}">
  <meta property="og:description" content="bestbeef gallery">
  <meta name="twitter:card" content="summary">
  <meta name="twitter:site" content="{{ url()->current() }}">
  <meta name="twitter:creator" content="">
  <meta name="twitter:title" content="gallery">
  <meta name="twitter:image" content="{{ url($gallery->image[0]->getUrl()) }}">
  <meta name="twitter:description" content="bestbeef gallery"> --}}
@endpush

<link href="{{ asset('css/frontend/detail.css?v') . time() }}" rel="stylesheet" />
<link href="{{ asset('plugins/fancyBox3/dist/jquery.fancybox.css?v=2.1.5') }}" rel="stylesheet" media="screen" />

<!-- GridGallery -->
<link rel="stylesheet" type="text/css" href="{{ asset('plugins/GridGallery/css/component.css') }}" />

@section('content')
  <!-- begin #content -->
  <div id="content" class="content">
    <div class="box-head">
      <h2 class="b-head" data-shadow="Gallery">{{ $pages->{get_lang('title')} }}</h2>
    </div>

    <div id="grid-gallery" class="grid-gallery item-more container">
      <section class="grid-wrap">
        <ul class="grid">
          <li class="grid-sizer"></li><!-- for Masonry column width -->
          @if(!empty($gallery->image))
            @foreach($gallery->image as $image)
              <li>
                <figure>
                  <a class="fancybox" href="{{ $image->getUrl() }}" data-fancybox="images-preview" data-thumbs='{"autoStart":true}'>
                    <div class="img">
                    <img src="{{$image->getUrl()}}" alt=""/>
                    </div>
                  </a>
                </figure>
              </li>
            @endforeach
          @endif
        </ul>
      </section>
    </div>
			
    <section class="box-social">
			<div class="container">
				<div class="b-social">
					<span>แชร์ไปยัง : </span>
					<ul class="social">
				    <li>
              <a class="I-fb" href="{{ get_link_share_facebook() }}" target="_blank" rel="noopener" aria-label="facebook"><img src="{{ asset('images/icon-fb.png') }}"></a>
            </li>
            <li>
              <a class="I-tw" href="{{ get_link_share_twitter() }}" target="_blank" rel="noopener" aria-label="twitter"><img src="{{ asset('images/icon-tw.png') }}"></a>
            </li>
					</ul>
				</div>
			</div>
		</section>

  </div>
  <!-- end #content -->
		
@endsection

@push('after-scripts')
<!-- fancybox3 -->
<script src="{{ asset('plugins/fancyBox3/dist/jquery.fancybox.min.js') }}"></script>

<!-- GridGallery -->
<script src="{{ asset('plugins/GridGallery/js/modernizr.custom.js') }}"></script>
<script src="{{ asset('plugins/GridGallery/js/imagesloaded.pkgd.min.js') }}"></script>
<script src="{{ asset('plugins/GridGallery/js/masonry.pkgd.min.js') }}"></script>
<script src="{{ asset('plugins/GridGallery/js/cbpGridGallery.js') }}"></script>

<script>
  $(document).ready(function() {
    new CBPGridGallery( document.getElementById('grid-gallery') );
  });
</script>

@endpush
