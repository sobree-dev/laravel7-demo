  <div class="row">
    <div class="col-md-12">
      <div class="form-group mt-2">
        <label for="image_detail">รูปรายละเอียด</label>
        <div class="needsclick dropzone" id="document-dropzone"></div>
      </div>
    </div> <!-- col6 -->
  </div> <!-- row -->
  <hr>
  <div class="form-group row mt-2">
    <div class="col-12 text-left">
      <button type="submit" class="btn btn-white"><i class="fa fa-save text-success"></i> บันทึกข้อมูล</button>
    </div>
  </div>


@push('after-scripts')
  <script>
    // Dropzone.autoDiscover = false;
    $(function(){

    });

    var uploadedImageDetailMap = {}
    Dropzone.options.documentDropzone = {
      url: '{!! route('backend.dropzone.upload') !!}',
      maxFilesize: 2, // MB
      addRemoveLinks: true,
      headers: {
        'X-CSRF-TOKEN': "{{ csrf_token() }}"
      },
      success: function (file, response) {
        $('form').append('<input type="hidden" name="image[]" value="' + response.name + '">')
        uploadedImageDetailMap[file.name] = response.name
      },
      removedfile: function (file) {
        file.previewElement.remove()
        var name = ''
        if (typeof file.file_name !== 'undefined') {
          name = file.file_name
        } else {
          name = uploadedDocumentMap[file.name]
        }
        $('form').find('input[name="image[]"][value="' + name + '"]').remove()
      },
      init: function () {
        @if(isset($gallery) && $gallery->image)
          var files =
            {!! json_encode($gallery->image) !!}
          for (var i in files) {
            var file = files[i]
            this.options.addedfile.call(this, file)
            file.previewElement.classList.add('dz-complete')
            $('form').append('<input type="hidden" name="image[]" value="' + file.file_name + '">')
          }
        @endif
      }
    }
  </script>
@endpush
       
