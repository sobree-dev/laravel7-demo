  <div class="row">
    <div class="col-md-12">
      <div class="row">
        <div class="col-xl-4 col-lg-4 col-md-12 col-sm-12 mt-2">
          <label class="col-form-label" for="title_th">ไตเติ้ล (ไทย) <span class="text-danger"> * </span> : </label>
          <input type="text" class="form-control" id="title_th" name="title_th" value="{{ old('title_th') ?? $about->title_th }}" required="" />
          {{ $errors->first('title_th') }}
        </div>

        <div class="col-xl-4 col-lg-4 col-md-12 col-sm-12 mt-2">
          <label class="col-form-label" for="title_en">ไตเติ้ล (อังกฤษ) <span class="text-danger"> * </span> : </label>
          <input type="text" class="form-control" id="title_en" name="title_en" value="{{ old('title_en') ?? $about->title_en }}" required="" />
          {{ $errors->first('title_en') }}
        </div>

        <div class="col-xl-4 col-lg-4 col-md-12 col-sm-12 mt-2">
          <label class="col-form-label" for="title_cn">ไตเติ้ล (จีน) <span class="text-danger"> * </span> : </label>
          <input type="text" class="form-control" id="title_cn" name="title_cn" value="{{ old('title_cn') ?? $about->title_cn }}" required="" />
          {{ $errors->first('title_cn') }}
        </div>
      </div>

      <div class="card border mt-2">
        <div class="card-header">ส่วนที่ 1</div>
        <div class="card-body">
          <div class="row">
            <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 mt-2">
              <label class="">รูปประกอบ <span class="text-danger">*</span> :</label>
              <div class="icon">
                <div class="uploaded_image">
                  <img id="preview_image1" src="{{ $about->image1 }}" class="img-icon" data-toggle="popover"  data-html='true'/>
                </div>
              </div>
              <div class="custom-file">
                <input type="file" 
                 class="image" id="image1" name="image1" >
                <label class="custom-file-label" for="image1">เลือกรูป</label>
              </div>
              <label class="text-pic">ขนาดภาพที่แนะนำ 1920x1080 (ขนาดไม่เกิน 5 MB)</label>
            </div>
          </div>

          <div class="row">
            <div class="col-xl-4 col-lg-4 col-md-12 col-sm-12 mt-2">
              <label class="col-form-label" for="description1_th">รายละเอียด (ไทย) <span class="text-danger"> * </span> : </label>
              <textarea name="description1_th" class="form-control" id="description1_th" name="description1_th"  required="" rows="4">{{ old('description1_th') ?? $about->description1_th }}</textarea>
              {{ $errors->first('description1_th') }}
            </div>

            <div class="col-xl-4 col-lg-4 col-md-12 col-sm-12 mt-2">
              <label class="col-form-label" for="description1_en">รายละเอียด (อังกฤษ) <span class="text-danger"> * </span> : </label>
              <textarea name="description1_en" class="form-control" id="description1_en" name="description1_en"  required="" rows="4">{{ old('description1_en') ?? $about->description1_en }}</textarea>
              {{ $errors->first('description1_en') }}
            </div>

            <div class="col-xl-4 col-lg-4 col-md-12 col-sm-12 mt-2">
              <label class="col-form-label" for="description1_cn">รายละเอียด (จีน) <span class="text-danger"> * </span> : </label>
              <textarea name="description1_cn" class="form-control" id="description1_cn" name="description1_cn"  required="" rows="4">{{ old('description1_cn') ?? $about->description1_cn }}</textarea>
              {{ $errors->first('description1_cn') }}
            </div>
          </div>
        </div>
      </div>

      <div class="card border mt-2">
        <div class="card-header">ส่วนที่ 2</div>
        <div class="card-body">
          <div class="row">
            <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 mt-2">
              <label class="">รูปประกอบ <span class="text-danger">*</span> :</label>
              <div class="icon">
                <div class="uploaded_image">
                  <img id="preview_image2" src="{{ $about->image2 }}" class="img-icon" data-toggle="popover"  data-html='true'/>
                </div>
              </div>
              <div class="custom-file">
                <input type="file" 
                 class="image" id="image2" name="image2" >
                <label class="custom-file-label" for="image2">เลือกรูป</label>
              </div>
              <label class="text-pic">ขนาดภาพที่แนะนำ 1920x1080 (ขนาดไม่เกิน 5 MB)</label>
            </div>
          </div>

          <div class="row">
            <div class="col-xl-4 col-lg-4 col-md-12 col-sm-12 mt-2">
              <label class="col-form-label" for="description2_th">รายละเอียด (ไทย) <span class="text-danger"> * </span> : </label>
              <textarea name="description2_th" class="form-control" id="description2_th" name="description2_th"  required="" rows="4">{{ old('description2_th') ?? $about->description2_th }}</textarea>
              {{ $errors->first('description2_th') }}
            </div>

            <div class="col-xl-4 col-lg-4 col-md-12 col-sm-12 mt-2">
              <label class="col-form-label" for="description2_en">รายละเอียด (อังกฤษ) <span class="text-danger"> * </span> : </label>
              <textarea name="description2_en" class="form-control" id="description2_en" name="description2_en"  required="" rows="4">{{ old('description2_en') ?? $about->description2_en }}</textarea>
              {{ $errors->first('description2_en') }}
            </div>

            <div class="col-xl-4 col-lg-4 col-md-12 col-sm-12 mt-2">
              <label class="col-form-label" for="description2_cn">รายละเอียด (จีน) <span class="text-danger"> * </span> : </label>
              <textarea name="description2_cn" class="form-control" id="description2_cn" name="description2_cn"  required="" rows="4">{{ old('description2_cn') ?? $about->description2_cn }}</textarea>
              {{ $errors->first('description2_cn') }}
            </div>
          </div>
        </div>
      </div>

      <div class="card border mt-2">
        <div class="card-header">ส่วนที่ 3</div>
        <div class="card-body">
          <div class="row">
            <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 mt-2">
              <label class="">รูปประกอบ <span class="text-danger">*</span> :</label>
              <div class="icon">
                <div class="uploaded_image">
                  <img id="preview_image3" src="{{ $about->image3 }}" class="img-icon" data-toggle="popover"  data-html='true'/>
                </div>
              </div>
              <div class="custom-file">
                <input type="file" 
                 class="image" id="image3" name="image3" >
                <label class="custom-file-label" for="image3">เลือกรูป</label>
              </div>
              <label class="text-pic">ขนาดภาพที่แนะนำ 1920x1080 (ขนาดไม่เกิน 5 MB)</label>
            </div>
          </div>

          <div class="row">
            <div class="col-xl-4 col-lg-4 col-md-12 col-sm-12 mt-2">
              <label class="col-form-label" for="description3_th">รายละเอียด (ไทย) <span class="text-danger"> * </span> : </label>
              <textarea name="description3_th" class="form-control" id="description3_th" name="description3_th"  required="" rows="4">{{ old('description3_th') ?? $about->description3_th }}</textarea>
              {{ $errors->first('description3_th') }}
            </div>

            <div class="col-xl-4 col-lg-4 col-md-12 col-sm-12 mt-2">
              <label class="col-form-label" for="description3_en">รายละเอียด (อังกฤษ) <span class="text-danger"> * </span> : </label>
              <textarea name="description3_en" class="form-control" id="description3_en" name="description3_en"  required="" rows="4">{{ old('description3_en') ?? $about->description3_en }}</textarea>
              {{ $errors->first('description3_en') }}
            </div>

            <div class="col-xl-4 col-lg-4 col-md-12 col-sm-12 mt-2">
              <label class="col-form-label" for="description3_cn">รายละเอียด (จีน) <span class="text-danger"> * </span> : </label>
              <textarea name="description3_cn" class="form-control" id="description3_cn" name="description3_cn"  required="" rows="4">{{ old('description3_cn') ?? $about->description3_cn }}</textarea>
              {{ $errors->first('description3_cn') }}
            </div>
          </div>
        </div>
      </div>

    </div> <!-- col6 -->
  </div> <!-- row -->
  <hr>
  <div class="form-group row mt-2">
    <div class="col-12 text-left">
      <button type="submit" class="btn btn-white"><i class="fa fa-save text-success"></i> บันทึกข้อมูล</button>
      <button type="reset" class="btn btn-white reset"><i class="fas fa-eraser text-warning"></i> ล้างข้อมูล</button>
      <button type="button" class="btn btn-white back" value="{{  url()->previous() }}"><i class="fas fa-reply text-danger" ></i> ย้อนกลับ</button>      
    </div>
  </div>
       
@push('after-scripts')
  <script>
  $('#image1').change(function() {
    $('#image1').removeData('imageWidth');
    $('#image1').removeData('imageHeight');
    var file = this.files[0];
    var tmpImg = new Image();
    tmpImg.src=window.URL.createObjectURL( file ); 
    tmpImg.onload = function() {
      width = tmpImg.naturalWidth,
      height = tmpImg.naturalHeight;
      $('#image1').data('imageWidth', width);
      $('#image1').data('imageHeight', height);
    }
  });

  $('#image2').change(function() {
    $('#image2').removeData('imageWidth');
    $('#image2').removeData('imageHeight');
    var file = this.files[0];
    var tmpImg = new Image();
    tmpImg.src=window.URL.createObjectURL( file ); 
    tmpImg.onload = function() {
      width = tmpImg.naturalWidth,
      height = tmpImg.naturalHeight;
      $('#image2').data('imageWidth', width);
      $('#image2').data('imageHeight', height);
    }
  });

  $('#image3').change(function() {
    $('#image3').removeData('imageWidth');
    $('#image3').removeData('imageHeight');
    var file = this.files[0];
    var tmpImg = new Image();
    tmpImg.src=window.URL.createObjectURL( file ); 
    tmpImg.onload = function() {
      width = tmpImg.naturalWidth,
      height = tmpImg.naturalHeight;
      $('#image3').data('imageWidth', width);
      $('#image3').data('imageHeight', height);
    }
  });

  $.validator.addMethod('ImageMaxWidth', function(value, element, maxWidth) {
    if(element.files.length == 0){
      return true; // check here if file not added than return true for not check file dimention
    }
    var width = $(element).data('imageWidth');
    if(width <= maxWidth){
      return true;
    }else{
      return false;
    }
  });

  $(function(){
    $('#form-validate').validate({
      rules: {
        image1: {
          ImageMaxWidth: 1920 //image max width 1920 px
        },
        image2: {
          ImageMaxWidth: 1920 //image max width 1920 px
        },
        image3: {
          ImageMaxWidth: 1920 //image max width 1920 px
        }
      },
      messages: {
        image1: {
          ImageMaxWidth: "ความกว้างของรูปไม่เกิน 1920 pixels"
        },
        image2: {
          ImageMaxWidth: "ความกว้างของรูปไม่เกิน 1920 pixels"
        },
        image3: {
          ImageMaxWidth: "ความกว้างของรูปไม่เกิน 1920 pixels"
        }
      },
      errorPlacement: function(error, element) {
        if($(element).attr('id') == 'image1' || $(element).attr('id') == 'image2' || $(element).attr('id') == 'image3'){
          error.insertAfter($(element).parent());
          $(element).siblings('.custom-file-label').toggleClass('error-border');
        } else {
          error.insertAfter(element);
        }
        
      }
    });

    $('#image1').on('change', function(){
       readURL(this, "preview_image1");
    });

    $('#image2').on('change', function(){
       readURL(this, "preview_image2");
    });

    $('#image3').on('change', function(){
       readURL(this, "preview_image3");
    });

  });
  </script>
@endpush