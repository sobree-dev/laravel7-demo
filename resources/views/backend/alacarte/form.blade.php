  <div class="row">
    <div class="col-md-12">
      <div class="row">
        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 mt-2">
          <label class="" for="detail_th">รายละเอียด (ไทย) : </label>
          <textarea name="detail_th" class="form-control" id="detail_th" name="detail_th" rows="4">{{  old('detail_th') ?? $alacarte->detail_th }}</textarea>
          {{ $errors->first('detail_th') }}
        </div>
      </div>

      <div class="row">
        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 mt-2">
          <label class="" for="detail_en">รายละเอียด (อังกฤษ) : </label>
          <textarea name="detail_en" class="form-control" id="detail_en" name="detail_en" rows="4">{{  old('detail_en') ?? $alacarte->detail_en }}</textarea>
          {{ $errors->first('detail_en') }}
        </div>
      </div>

      <div class="row">
        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 mt-2">
          <label class="" for="detail_cn">รายละเอียด (จีน) : </label>
          <textarea name="detail_cn" class="form-control" id="detail_cn" name="detail_cn" rows="4">{{  old('detail_cn') ?? $alacarte->detail_cn }}</textarea>
          {{ $errors->first('detail_cn') }}
        </div>
      </div>

      <div class="row">
        <div class="col-xl-6 col-md-12 col-lg-6">
          <label class="col-form-label" style='width: 100%;'>สถานะการใช้งาน : </label>         
          <div class="radio radio-css radio-inline Me-2">
            @if(!empty($food_type) && $food_type->active == 'Inactive') 
              <input class="form-check-input" type="radio" name="active" id="active1" value="1" >
              <label class="form-check-label ml-2" for="active1">เปิดใช้งาน</label>
            </div>
            <div class="radio radio-css radio-inline">
              <input class="form-check-input" type="radio" name="active" id="active2" value="0" checked>
              <label class="form-check-label ml-2" for="active2">ปิดใช้งาน</label>
            </div>
             
           @else
              <input class="form-check-input" type="radio" name="active" id="active1" value="1" checked>
              <label class="form-check-label ml-2" for="active1">เปิดใช้งาน</label>
            </div>
            <div class="radio radio-css radio-inline">
              <input class="form-check-input" type="radio" name="active" id="_IsActive2" value="0" >
              <label class="form-check-label ml-2" for="_IsActive2">ปิดใช้งาน</label>
            </div>         
          @endif
        </div>
      </div>

    </div> <!-- col6 -->
  </div> <!-- row -->
  <hr>
  <div class="form-group row mt-2">
    <div class="col-12 text-left">
      <button type="submit" class="btn btn-white"><i class="fa fa-save text-success"></i> บันทึกข้อมูล</button>
      <button type="reset" class="btn btn-white reset"><i class="fas fa-eraser text-warning"></i> ล้างข้อมูล</button>
      <button type="button" class="btn btn-white back" value="{{  url()->previous() }}"><i class="fas fa-reply text-danger" ></i> ย้อนกลับ</button>
    </div>
  </div>


@push('after-scripts')
  <script>
  $(function(){
    $('#form-validate').validate({
      errorPlacement: function(error, element) {
        if($(element).attr('id') == 'image'){
          error.insertAfter($(element).parent());
          $(element).siblings('.custom-file-label').toggleClass('error-border');
        } else {
          error.insertAfter(element);
        }
        
      }
    });

    CKEDITOR.replace('detail_th', {
      allowedContent: true,
      filebrowserUploadUrl: "{{route('backend.ckeditor.upload', ['_token' => csrf_token() ])}}",
      filebrowserUploadMethod: 'form'
    });

    CKEDITOR.replace('detail_en', {
      allowedContent: true,
      filebrowserUploadUrl: "{{route('backend.ckeditor.upload', ['_token' => csrf_token() ])}}",
      filebrowserUploadMethod: 'form'
    });

    CKEDITOR.replace('detail_cn', {
      allowedContent: true,
      filebrowserUploadUrl: "{{route('backend.ckeditor.upload', ['_token' => csrf_token() ])}}",
      filebrowserUploadMethod: 'form'
    });
  });
  </script>
@endpush
       
