  <div class="row">
    <div class="col-md-12">
      <div class="row">
        <div class="col-xl-3 col-md-12 col-sm-12 mt-2">
          <label class='col-form-label' for="title_en">ประเภทรีวิว <span class="text-danger"> * </span> : </label>
          <select id="type" name="type" class="form-control">
            <option value="customer" {{ !empty($review->type) && $review->type == 'customer' ? 'selected' : '' }}>Customer</option>
            <option value="blogger" {{ !empty($review->type) && $review->type == 'blogger' ? 'selected' : '' }}>Blogger</option>
            <option value="youtube" {{ !empty($review->type) && $review->type == 'youtube' ? 'selected' : '' }}>Youtube</option>
          </select>
        </div>
      </div>

      <div class="row">
        <div class="col-xl-6 col-md-12 col-sm-12 mt-2 col-lg-6">
          <label class="">รูปคนรีวิว <span class="text-danger">*</span> :</label>
          <div class="icon">
            <div class="uploaded_image">
              <img id="preview_image_user" src="{{ url($review->image_user) ?? '' }}" class="img-icon" data-toggle="popover" data-html='true'/>
            </div>
          </div>
          
          <div class="custom-file">
            <input type="file" 
             class="image" id="image_user" name="image_user" {{request()->route()->getActionMethod() == 'create' ? 'required' : ''}}>
            <label class="custom-file-label" for="image_user">เลือกรูป</label>
          </div>
          <label class='text-pic'>ขนาดภาพที่แนะนำ 140x140 (ขนาดไม่เกิน 1 MB)</label>
          {{ $errors->first('image_user') }}
        </div>

        <div class="col-xl-6 col-md-12 col-sm-12 mt-2 col-lg-6">
          <label class="">รูป cover <span class="text-danger">*</span> :</label>
          <div class="icon">
            <div class="uploaded_image">
              <img id="preview_image" src="{{ url($review->image) ?? '' }}" class="img-icon" data-toggle="popover"  data-html='true'/>
            </div>
          </div>
          
          <div class="custom-file">
            <input type="file" 
             class="image" id="image" name="image">
            <label class="custom-file-label" for="image">เลือกรูป</label>
          </div>
          <label class='text-pic'>ขนาดภาพที่แนะนำ 1920x1080 (ขนาดไม่เกิน 5 MB)</label>
          {{ $errors->first('image') }}
        </div>
      </div>

      <div class="row">
        <div class="col-xl-4 col-lg-4 col-md-12 col-sm-12 mt-2">
          <label class="col-form-label" for="title_th">ชื่อ/หัวข้อ (ไทย) <span class="text-danger"> * </span> : </label>
          <input type="text" class="form-control" id="title_th" name="title_th" value="{{  old('title_th') ?? $review->title_th }}" required="" />
          {{ $errors->first('title_th') }}
        </div>

        <div class="col-xl-4 col-lg-4 col-md-12 col-sm-12 mt-2">
          <label class="col-form-label" for="title_en">ชื่อ/หัวข้อ (อังกฤษ) <span class="text-danger"> * </span> : </label>
          <input type="text" class="form-control" id="title_en" name="title_en" value="{{  old('title_en') ?? $review->title_en }}" required="" />
          {{ $errors->first('title_en') }}
        </div>

        <div class="col-xl-4 col-lg-4 col-md-12 col-sm-12 mt-2">
          <label class="col-form-label" for="title_cn">ชื่อ/หัวข้อ (จีน) <span class="text-danger"> * </span> : </label>
          <input type="text" class="form-control" id="title_cn" name="title_cn" value="{{  old('title_cn') ?? $review->title_cn }}" required="" />
          {{ $errors->first('title_cn') }}
        </div>
      </div>

      <div class="row">
        <div class="col-xl-4 col-lg-4 col-md-12 col-sm-12 mt-2">
          <label class="col-form-label" for="short_description_th">รายละเอียดอย่างสั้น (ไทย) <span class="text-danger"> * </span> : </label>
          <textarea class="form-control text-count-150" id="short_description_th" name="short_description_th" rows="2" required="">{{  old('short_description_th') ?? $review->short_description_th }}</textarea>
          {{ $errors->first('short_description_th') }}
        </div>

        <div class="col-xl-4 col-lg-4 col-md-12 col-sm-12 mt-2">
          <label class="col-form-label" for="short_description_en">รายละเอียดอย่างสั้น (อังกฤษ) <span class="text-danger"> * </span> : </label>
          <textarea class="form-control text-count-150" id="short_description_en" name="short_description_en" rows="2" required="">{{  old('short_description_en') ?? $review->short_description_en }}</textarea>
          {{ $errors->first('short_description_en') }}
        </div>

        <div class="col-xl-4 col-lg-4 col-md-12 col-sm-12 mt-2">
          <label class="col-form-label" for="short_description_cn">รายละเอียดอย่างสั้น (จีน) <span class="text-danger"> * </span> : </label>
          <textarea class="form-control text-count-150" id="short_description_cn" name="short_description_cn" rows="2" required="">{{  old('short_description_cn') ?? $review->short_description_cn }}</textarea>
          {{ $errors->first('short_description_cn') }}
        </div>
      </div>

      <div class="row">
        <div class="col-xl-12 col-md-12 col-sm-12 mt-2">
          <label class="" for="description_th">รายละเอียด (ไทย) <span class="text-danger"> * </span> : </label>
          <textarea class="form-control " id="description_th" name="description_th" rows="4">{{  old('description_th') ?? $review->description_th }}</textarea>
          {{ $errors->first('description_th') }}
        </div>

        <div class="col-xl-12 col-md-12 col-sm-12 mt-2">
          <label class="" for="description_en">รายละเอียด (อังกฤษ) <span class="text-danger"> * </span> : </label>
          <textarea class="form-control " id="description_en" name="description_en" rows="4">{{  old('description_en') ?? $review->description_en }}</textarea>
          {{ $errors->first('description_en') }}
        </div>

        <div class="col-xl-12 col-md-12 col-sm-12 mt-2">
          <label class="" for="description_cn">รายละเอียด (จีน) <span class="text-danger"> * </span> : </label>
          <textarea class="form-control " id="description_cn" name="description_cn" rows="4">{{  old('description_cn') ?? $review->description_cn }}</textarea>
          {{ $errors->first('description_cn') }}
        </div>
      </div>

      <div class="row">
        <div class="col-xl-2 col-lg-2 col-md-12 col-sm-12 mt-2">
          <label class='col-form-label' for="title_en">คะแนน <span class="text-danger"> * </span> : </label>
          <select id="point" name="point" class="form-control" required="">
            <option value="">กรุณาระบุ</option>
            @for($i = 1; $i <= 10; $i++)
              <option value="{{ $i }}" {{ $review->point == $i ? 'selected' : '' }}>{{ $i }}</option>
            @endfor
          </select>
        </div>
      </div>
      
      <div class="form-group mt-2">
        <label for="image_detail">รูปประกอบ</label>
        <div class="needsclick dropzone" id="document-dropzone"></div>
      </div>

      <div class="row">
        <div class="col-xl-6 col-md-12 col-lg-6 mt-2">
          <label class="col-form-label" style='width: 100%;'>สถานะการใช้งาน : </label>
          @if(!empty($review) && $review->active == 'Inactive')
            <div class="radio radio-css radio-inline">
              <input class="form-check-input" type="radio" name="active" id="active1" value="1">
              <label class="form-check-label ml-2" for="active1">เปิดใช้งาน</label>
            </div>
            <div class="radio radio-css radio-inline">
              <input class="form-check-input" type="radio" name="active" id="active2" value="0" checked>
              <label class="form-check-label ml-2" for="active2">ปิดใช้งาน</label>
            </div>
           @else
            <div class="radio radio-css radio-inline">
              <input class="form-check-input" type="radio" name="active" id="active1" value="1" checked>
              <label class="form-check-label ml-2" for="active1">เปิดใช้งาน</label>
            </div>
            <div class="radio radio-css radio-inline">
              <input class="form-check-input" type="radio" name="active" id="_IsActive2" value="0">
              <label class="form-check-label ml-2" for="_IsActive2">ปิดใช้งาน</label>
            </div>
          @endif
        </div>
      </div>

    </div> <!-- col6 -->
  </div> <!-- row -->
  <hr>
  <div class="form-group row">
    <div class="col-12 text-left">
      <button type="submit" class="btn btn-white"><i class="fa fa-save text-success"></i> บันทึกข้อมูล</button>
      <button type="reset" class="btn btn-white reset"><i class="fas fa-eraser text-warning"></i> ล้างข้อมูล</button>
      <button type="button" class="btn btn-white back" value="{{  url()->previous() }}"><i class="fas fa-reply text-danger" ></i> ย้อนกลับ</button>      
    </div>
  </div>


@push('after-scripts')
  <script>
  // Dropzone.autoDiscover = false;
  $('#image_user').change(function() {
    $('#image_user').removeData('imageWidth');
    $('#image_user').removeData('imageHeight');
    var file = this.files[0];
    var tmpImg = new Image();
    tmpImg.src=window.URL.createObjectURL( file ); 
    tmpImg.onload = function() {
      width = tmpImg.naturalWidth,
      height = tmpImg.naturalHeight;
      $('#image_user').data('imageWidth', width);
      $('#image_user').data('imageHeight', height);
    }
  });

  $('#image').change(function() {
    $('#image').removeData('imageWidth');
    $('#image').removeData('imageHeight');
    var file = this.files[0];
    var tmpImg = new Image();
    tmpImg.src=window.URL.createObjectURL( file ); 
    tmpImg.onload = function() {
      width = tmpImg.naturalWidth,
      height = tmpImg.naturalHeight;
      $('#image').data('imageWidth', width);
      $('#image').data('imageHeight', height);
    }
  });

  $.validator.addMethod('ImageMaxWidth', function(value, element, maxWidth) {
    if(element.files.length == 0){
      return true; // check here if file not added than return true for not check file dimention
    }
    var width = $(element).data('imageWidth');
    if(width <= maxWidth){
      return true;
    }else{
      return false;
    }
  });

  $(function(){
    $('#form-validate').validate({
      rules: {
        image_user: {
          ImageMaxWidth: 140 //image max width 140 px
        },
        image: {
          ImageMaxWidth: 1920 //image max width 1920 px
        }
      },
      messages: {
        image_user: {
          ImageMaxWidth: "ความกว้างของรูปไม่เกิน 140 pixels"
        },
        image: {
          ImageMaxWidth: "ความกว้างของรูปไม่เกิน 1920 pixels"
        }
      },
      errorPlacement: function(error, element) {
        if($(element).attr('id') == 'image_user' || $(element).attr('id') == 'image'){
          error.insertAfter($(element).parent());
          $(element).siblings('.custom-file-label').toggleClass('error-border');
        } else {
          error.insertAfter(element);
        }
        
      }
    });

    $('#image_user').on('change', function(){
      readURL(this, "preview_image_user");
    });

    $('#image').on('change', function(){
      readURL(this, "preview_image");
    });

    $('.text-count-150').characterCounter({
      minlength: 0,
      maxlength: 150,
      blockextra: true,
      position: 'top',
    });

    CKEDITOR.replace('description_th', {
      allowedContent: true,
      filebrowserUploadUrl: "{{route('backend.ckeditor.upload', ['_token' => csrf_token() ])}}",
      filebrowserUploadMethod: 'form'
    });

    CKEDITOR.replace('description_en', {
      allowedContent: true,
      filebrowserUploadUrl: "{{route('backend.ckeditor.upload', ['_token' => csrf_token() ])}}",
      filebrowserUploadMethod: 'form'
    });

    CKEDITOR.replace('description_cn', {
      allowedContent: true,
      filebrowserUploadUrl: "{{route('backend.ckeditor.upload', ['_token' => csrf_token() ])}}",
      filebrowserUploadMethod: 'form'
    });
  });

  var uploadedImageDetailMap = {}
  Dropzone.options.documentDropzone = {
    url: '{!! route('backend.dropzone.upload') !!}',
    maxFilesize: 5, // MB
    addRemoveLinks: true,
    headers: {
      'X-CSRF-TOKEN': "{{ csrf_token() }}"
    },
    success: function (file, response) {
      $('form').append('<input type="hidden" name="image_detail[]" value="' + response.name + '">')
      uploadedImageDetailMap[file.name] = response.name
    },
    removedfile: function (file) {
      file.previewElement.remove()
      var name = ''
      if (typeof file.file_name !== 'undefined') {
        name = file.file_name
      } else {
        name = uploadedDocumentMap[file.name]
      }
      $('form').find('input[name="image_detail[]"][value="' + name + '"]').remove()
    },
    init: function () {
      @if(isset($review) && $review->image_detail)
        var files =
          {!! json_encode($review->image_detail) !!}
        for (var i in files) {
          var file = files[i]
          this.options.addedfile.call(this, file)
          file.previewElement.classList.add('dz-complete')
          $('form').append('<input type="hidden" name="image_detail[]" value="' + file.file_name + '">')
        }
      @endif
    }
  }
  </script>
@endpush
       
