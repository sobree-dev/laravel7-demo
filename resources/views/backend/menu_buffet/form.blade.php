  <div class="row">
    <div class="col-md-12">
      <div class="row">
        <div class="col-xl-4 col-lg-4 col-md-12 col-sm-12 mt-2">
          <label class="col-form-label" for="end_date">ประเภทบุฟเฟ่ต์ <span class="text-danger"> * </span> : </label>
          <select name="buffet_type_id" id="buffet_type_id" class="form-control" required>
            <option value="">กรุณาระบุ</option>
            @foreach($buffet_types as $buffet_type)
              <option value="{{ $buffet_type->id }}" {{ $menu_buffet->buffet_type_id == $buffet_type->id ? 'selected' : '' }}>{{ $buffet_type->name_th }}</option>
            @endforeach
          </select>
        </div>

        <div class="col-xl-4 col-lg-4 col-md-12 col-sm-12 mt-2">
          <label class="col-form-label" for="end_date">ประเภทอาหาร <span class="text-danger"> * </span> : </label>
          <select name="food_type_id" id="food_type_id" class="form-control" required>
            <option value="">กรุณาระบุ</option>
            @foreach($food_types as $food_type)
              <option value="{{ $food_type->id }}" {{ $menu_buffet->food_type_id == $food_type->id ? 'selected' : '' }}>{{ $food_type->name_th }}</option>
            @endforeach
          </select>
        </div>
      </div>

      <div class="row">
        <div class="col-xl-4 col-lg-4 col-md-12 col-sm-12 mt-2">
          <label class="col-form-label" for="name_th">ชื่ออาหาร (ไทย) <span class="text-danger"> * </span> : </label>
          <input type="text" class="form-control" id="name_th" name="name_th" value="{{ old('name_th') ?? $menu_buffet->name_th }}" required="" />
          {{ $errors->first('name_th') }}
        </div>

        <div class="col-xl-4 col-lg-4 col-md-12 col-sm-12 mt-2">
          <label class="col-form-label" for="name_en">ชื่ออาหาร (อังกฤษ) <span class="text-danger"> * </span> : </label>
          <input type="text" class="form-control" id="name_en" name="name_en" value="{{ old('name_en') ?? $menu_buffet->name_en }}" required="" />
          {{ $errors->first('name_en') }}
        </div>

        <div class="col-xl-4 col-lg-4 col-md-12 col-sm-12 mt-2">
          <label class="col-form-label" for="name_cn">ชื่ออาหาร (จีน) <span class="text-danger"> * </span> : </label>
          <input type="text" class="form-control" id="name_cn" name="name_cn" value="{{ old('name_cn') ?? $menu_buffet->name_cn }}" required="" />
          {{ $errors->first('name_cn') }}
        </div>
      </div>

      <div class="row">
        <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 mt-2">
          <label class="">รูป cover <span class="text-danger">*</span> :</label>
          <div class="icon">
            <div class="uploaded_image">
              <img id="preview_image" src="{{ $menu_buffet->image ?? '' }}" class="img-icon" data-toggle="popover"  data-html="true"/>
            </div>
          </div>
          
          <div class="custom-file">
            <input type="file" 
             class="image" id="image" name="image" {{request()->route()->getActionMethod() == 'create' ? 'required' : ''}}>
            <label class="custom-file-label" for="image" >เลือกรูป</label>
          </div>
          <label class='text-pic'>ขนาดภาพที่แนะนำ 1920x1080 (ขนาดไม่เกิน 5 MB)</label>
            {{ $errors->first('image') }}
        </div>
      </div>

      <div class="row">
        <div class="col-xl-6 col-lg-6 col-md-12 mt-2">
          <div class="custom-control custom-checkbox">
            <input type="checkbox" class="custom-control-input" id="recommended_check" value="1" {{ $menu_buffet->recommended == '1' ? 'checked' : '' }}>
            <label class="custom-control-label pt-1" for="recommended_check">แสดงเป็นสินค้าแนะนำ</label>
            <input type="hidden" id="recommended" name="recommended" value="{{ $menu_buffet->recommended }}">
          </div>
        </div>
      </div>

      <div class="row">
        <div class="col-xl-6 col-md-12 col-lg-6">
          <label class="col-form-label" style='width: 100%;'>สถานะการใช้งาน : </label>         
          <div class="radio radio-css radio-inline Me-2">
            @if(!empty($menu_buffet) && $menu_buffet->active == 'Inactive') 
              <input class="form-check-input" type="radio" name="active" id="active1" value="1" >
              <label class="form-check-label ml-2" for="active1">เปิดใช้งาน</label>
            </div>
            <div class="radio radio-css radio-inline">
              <input class="form-check-input" type="radio" name="active" id="active2" value="0" checked>
              <label class="form-check-label ml-2" for="active2">ปิดใช้งาน</label>
            </div>
             
           @else
              <input class="form-check-input" type="radio" name="active" id="active1" value="1" checked>
              <label class="form-check-label ml-2" for="active1">เปิดใช้งาน</label>
            </div>
            <div class="radio radio-css radio-inline">
              <input class="form-check-input" type="radio" name="active" id="_IsActive2" value="0" >
              <label class="form-check-label ml-2" for="_IsActive2">ปิดใช้งาน</label>
            </div>         
          @endif
        </div>
      </div>

    </div> <!-- col6 -->
  </div> <!-- row -->
  <hr>
  <div class="form-group row mt-2">
    <div class="col-12 text-left">
      <button type="submit" class="btn btn-white"><i class="fa fa-save text-success"></i> บันทึกข้อมูล</button>
      <button type="reset" class="btn btn-white reset"><i class="fas fa-eraser text-warning"></i> ล้างข้อมูล</button>
      <button type="button" class="btn btn-white back" value="{{  url()->previous() }}"><i class="fas fa-reply text-danger" ></i> ย้อนกลับ</button>
    </div>
  </div>

@if(!empty($menu_buffet->buffet_types))
  @php
    $buffet_types = $menu_buffet->buffet_types->pluck('name')->toJson();
  @endphp
@else
  @php
    $buffet_types = '';
  @endphp
@endif

@push('after-scripts')
  <script>
  $('#recommended_check').click(function() {
    if ($('#recommended_check').is(':checked')) {
      $('#recommended').val(1);
    } else {
      $('#recommended').val(0);
    }
  });
  
  $('#image').change(function() {
    $('#image').removeData('imageWidth');
    $('#image').removeData('imageHeight');
    var file = this.files[0];
    var tmpImg = new Image();
    tmpImg.src=window.URL.createObjectURL( file ); 
    tmpImg.onload = function() {
      width = tmpImg.naturalWidth,
      height = tmpImg.naturalHeight;
      $('#image').data('imageWidth', width);
      $('#image').data('imageHeight', height);
    }
  });

  $.validator.addMethod('ImageMaxWidth', function(value, element, maxWidth) {
    if(element.files.length == 0){
      return true; // check here if file not added than return true for not check file dimention
    }
    var width = $(element).data('imageWidth');
    if(width <= maxWidth){
      return true;
    }else{
      return false;
    }
  });

  $(function(){
    // var buffet_types = '{!! $buffet_types !!}';
    // $('#buffet_type_id').select2();
    // if(buffet_types != '') {
    //   $('#buffet_type_id').val($.parseJSON(buffet_types)).change();
    // }

    $('#form-validate').validate({
      rules: {
        image: {
          ImageMaxWidth: 1920 //image max width 1920 px
        }
      },
      messages: {
        image: {
          ImageMaxWidth: "ความกว้างของรูปไม่เกิน 1920 pixels"
        }
      },
      errorPlacement: function(error, element) {
        if($(element).attr('id') == 'image'){
          error.insertAfter($(element).parent());
          $(element).siblings('.custom-file-label').toggleClass('error-border');
        } else {
          error.insertAfter(element);
        }
        
      }
    });

    $('#image').on('change', function(){
      readURL(this, "preview_image");
    });
  });
  </script>
@endpush
       
