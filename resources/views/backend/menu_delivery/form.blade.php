  <div class="row">
    <div class="col-md-12">
      <div class="row">
        <div class="col-xl-4 col-lg-4 col-md-12 col-sm-12 mt-2">
          <label class="col-form-label" for="name_th">ชื่ออาหาร (ไทย) <span class="text-danger"> * </span> : </label>
          <input type="text" class="form-control" id="name_th" name="name_th" value="{{ old('name_th') ?? $menu_delivery->name_th }}" required="" />
          {{ $errors->first('name_th') }}
        </div>

        <div class="col-xl-4 col-lg-4 col-md-12 col-sm-12 mt-2">
          <label class="col-form-label" for="name_en">ชื่ออาหาร (อังกฤษ) <span class="text-danger"> * </span> : </label>
          <input type="text" class="form-control" id="name_en" name="name_en" value="{{ old('name_en') ?? $menu_delivery->name_en }}" required="" />
          {{ $errors->first('name_en') }}
        </div>

        <div class="col-xl-4 col-lg-4 col-md-12 col-sm-12 mt-2">
          <label class="col-form-label" for="name_cn">ชื่ออาหาร (จีน) <span class="text-danger"> * </span> : </label>
          <input type="text" class="form-control" id="name_cn" name="name_cn" value="{{ old('name_cn') ?? $menu_delivery->name_cn }}" required="" />
          {{ $errors->first('name_cn') }}
        </div>
      </div>

      <div class="row">
        <div class="col-xl-4 col-lg-4 col-md-12 col-sm-12 mt-2">
          <label class="col-form-label" for="description_th">คำอธิบาย (ไทย) : </label>
          <textarea name="description_th" class="form-control" id="description_th" name="description_th" rows="2">{{  old('description_th') ?? $menu_delivery->description_th }}</textarea>
          {{ $errors->first('description_th') }}
        </div>

        <div class="col-xl-4 col-lg-4 col-md-12 col-sm-12 mt-2">
          <label class="col-form-label" for="description_en">คำอธิบาย (อังกฤษ) : </label>
          <textarea name="description_en" class="form-control" id="description_en" name="description_en" rows="2">{{  old('description_en') ?? $menu_delivery->description_en }}</textarea>
          {{ $errors->first('description_en') }}
        </div>

        <div class="col-xl-4 col-lg-4 col-md-12 col-sm-12 mt-2">
          <label class="col-form-label" for="description_cn">คำอธิบาย (จีน) : </label>
          <textarea name="description_cn" class="form-control" id="description_cn" name="description_cn" rows="2">{{  old('description_cn') ?? $menu_delivery->description_cn }}</textarea>
          {{ $errors->first('description_cn') }}
        </div>
      </div>

      <div class="row">
        <div class="col-xl-4 col-lg-4 col-md-12 col-sm-12 mt-2">
          <label class="col-form-label" for="price">ราคา <span class="text-danger"> * </span> : </label>
          <input type="number" class="form-control" id="price" name="price" value="{{ old('price') ?? $menu_delivery->price }}" required="" />
          {{ $errors->first('price') }}
        </div>
      </div>

      <div class="row">
        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 mt-2">
          <label class="" for="detail_th">รายละเอียด (ไทย) : </label>
          <textarea name="detail_th" class="form-control" id="detail_th" name="detail_th" rows="4">{{  old('detail_th') ?? $menu_delivery->detail_th }}</textarea>
          {{ $errors->first('detail_th') }}
        </div>
      </div>

      <div class="row">
        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 mt-2">
          <label class="" for="detail_en">รายละเอียด (อังกฤษ) : </label>
          <textarea name="detail_en" class="form-control" id="detail_en" name="detail_en" rows="4">{{  old('detail_en') ?? $menu_delivery->detail_en }}</textarea>
          {{ $errors->first('detail_en') }}
        </div>
      </div>

      <div class="row">
        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 mt-2">
          <label class="" for="detail_cn">รายละเอียด (จีน) : </label>
          <textarea name="detail_cn" class="form-control" id="detail_cn" name="detail_cn" rows="4">{{  old('detail_cn') ?? $menu_delivery->detail_cn }}</textarea>
          {{ $errors->first('detail_cn') }}
        </div>
      </div>

      <div class="row">
        <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 mt-2">
          <label class="">รูป cover <span class="text-danger">*</span> :</label>
          <div class="icon">
            <div class="uploaded_image">
              <img id="preview_image" src="{{ $menu_delivery->image ?? '' }}" class="img-icon" data-toggle="popover"  data-html="true"/>
            </div>
          </div>
          <div class="custom-file">
            <input type="file" 
             class="image" id="image" name="image" {{request()->route()->getActionMethod() == 'create' ? 'required' : ''}}>
            <label class="custom-file-label" for="image" >เลือกรูป</label>
          </div>
          <label class='text-pic'>ขนาดภาพที่แนะนำ 1920x1080 (ขนาดไม่เกิน 5 MB)</label>
          {{ $errors->first('image') }}
        </div>
      </div>

      <div class="row">
        <div class="col-xl-6 col-lg-6 col-md-12 mt-2">
          <label class="col-form-label" style='width: 100%;'>สถานะการใช้งาน : </label>         
          @if(!empty($menu_delivery) && $menu_delivery->active == 'Inactive') 
            <div class="radio radio-css radio-inline">
              <input class="form-check-input" type="radio" name="active" id="active1" value="1" >
              <label class="form-check-label ml-2" for="active1">เปิดใช้งาน</label>
            </div>
            <div class="radio radio-css radio-inline">
              <input class="form-check-input" type="radio" name="active" id="active2" value="0" checked>
              <label class="form-check-label ml-2" for="active2">ปิดใช้งาน</label>
            </div>
          @else
            <div class="radio radio-css radio-inline">
              <input class="form-check-input" type="radio" name="active" id="active1" value="1" checked>
              <label class="form-check-label ml-2" for="active1">เปิดใช้งาน</label>
            </div>
            <div class="radio radio-css radio-inline">
              <input class="form-check-input" type="radio" name="active" id="_IsActive2" value="0" >
              <label class="form-check-label ml-2" for="_IsActive2">ปิดใช้งาน</label>
            </div>         
          @endif
        </div>
      </div>

    </div> <!-- col6 -->
  </div> <!-- row -->
  <hr>
  <div class="form-group row mt-2">
    <div class="col-12 text-left">
      <button type="submit" class="btn btn-white"><i class="fa fa-save text-success"></i> บันทึกข้อมูล</button>
      <button type="reset" class="btn btn-white reset"><i class="fas fa-eraser text-warning"></i> ล้างข้อมูล</button>
      <button type="button" class="btn btn-white back" value="{{  url()->previous() }}"><i class="fas fa-reply text-danger" ></i> ย้อนกลับ</button>
    </div>
  </div>

@push('after-scripts')
  <script>
  $('#recommended_check').click(function() {
    if ($('#recommended_check').is(':checked')) {
      $('#recommended').val(1);
    } else {
      $('#recommended').val(0);
    }
  });

  $('#image').change(function() {
    $('#image').removeData('imageWidth');
    $('#image').removeData('imageHeight');
    var file = this.files[0];
    var tmpImg = new Image();
    tmpImg.src=window.URL.createObjectURL( file ); 
    tmpImg.onload = function() {
      width = tmpImg.naturalWidth,
      height = tmpImg.naturalHeight;
      $('#image').data('imageWidth', width);
      $('#image').data('imageHeight', height);
    }
  });

  $.validator.addMethod('ImageMaxWidth', function(value, element, maxWidth) {
    if(element.files.length == 0){
      return true; // check here if file not added than return true for not check file dimention
    }
    var width = $(element).data('imageWidth');
    if(width <= maxWidth){
      return true;
    }else{
      return false;
    }
  });

  $(function(){
    $('#form-validate').validate({
      rules: {
        image: {
          ImageMaxWidth: 1920 //image max width 1920 px
        }
      },
      messages: {
        image: {
          ImageMaxWidth: "ความกว้างของรูปไม่เกิน 1920 pixels"
        }
      },
      errorPlacement: function(error, element) {
        if($(element).attr('id') == 'image'){
          error.insertAfter($(element).parent());
          $(element).siblings('.custom-file-label').toggleClass('error-border');
        } else {
          error.insertAfter(element);
        }
        
      }
    });

    $('#image').on('change', function(){
      readURL(this, "preview_image");
    });

    CKEDITOR.replace('detail_th', {
      allowedContent: true,
      filebrowserUploadUrl: "{{route('backend.ckeditor.upload', ['_token' => csrf_token() ])}}",
      filebrowserUploadMethod: 'form'
    });

    CKEDITOR.replace('detail_en', {
      allowedContent: true,
      filebrowserUploadUrl: "{{route('backend.ckeditor.upload', ['_token' => csrf_token() ])}}",
      filebrowserUploadMethod: 'form'
    });

    CKEDITOR.replace('detail_cn', {
      allowedContent: true,
      filebrowserUploadUrl: "{{route('backend.ckeditor.upload', ['_token' => csrf_token() ])}}",
      filebrowserUploadMethod: 'form'
    });
  });
  </script>
@endpush
       
