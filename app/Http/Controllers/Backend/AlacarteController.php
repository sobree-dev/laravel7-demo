<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\Alacarte;
use Illuminate\Support\Facades\Auth;

class AlacarteController extends Controller
{
  const MODULE = 'menu_alacarte';

  public function index(Request $request) 
  {
    $this->authorize(mapPermission(self::MODULE));
    $alacarte = Alacarte::find(1)->first();

    return view('backend.alacarte.update', compact('alacarte'));
  }

  public function update(Request $request, Alacarte $alacarte) {
    $this->authorize(mapPermission(self::MODULE));
    $alacarte->update($this->validateRequest());

    return redirect(route('backend.alacarte.index'));
  }

  public function destroy(Alacarte $alacarte) {
    $this->authorize(mapPermission(self::MODULE));
    $alacarte->delete();

    return redirect(route('backend.alacarte.index'));
  }

  private function validateRequest() {
    $validatedData = request()->validate([
      "detail_th"   => "required",
      "detail_en"   => "required",
      "detail_cn"   => "required",
      "active" => "",
    ]);

    request()->validate([
      "image"  => ['sometimes', 'file','image','max:1000'],
    ]);

    $validatedData['updated_by'] = Auth::id();
    if(request()->route()->getActionMethod() == 'store') :
      $validatedData['created_by'] = Auth::id();
    endif;

    return $validatedData;
  }

}

