<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\Delivery;
use Illuminate\Support\Facades\Auth;

class DeliveryController extends Controller
{
  const MODULE = 'delivery';

  public function index(Request $request) 
  {
    $this->authorize(mapPermission(self::MODULE));
    if ($request->filled('keyword')):
      $deliverys = Delivery::getDataByKeyword($request->keyword)->get();
    else:
      $deliverys = Delivery::limit(50)->get();
    endif;

    return view('backend.delivery.index', compact('deliverys'));
  }

  public function search(Request $request) {
    $this->authorize(mapPermission(self::MODULE));
    if ($request->filled('keyword')):
      $deliverys = Delivery::getDataByKeyword($request->keyword)->get();
    else:
      $deliverys = Delivery::all();
    endif;

    return view('backend.delivery.show', compact('deliverys'));
  }

  public function create() {
    $this->authorize(mapPermission(self::MODULE));
    $delivery = new Delivery;

    return view('backend.delivery.create', compact('delivery'));

  }

  public function store(Request $request) {
    $this->authorize(mapPermission(self::MODULE));
    $delivery = Delivery::create($this->validateRequest());
    $delivery->storeImage();

    return redirect(route('backend.delivery.index'));
  }

  public function edit(Delivery $delivery) {
    $this->authorize(mapPermission(self::MODULE));

    return view('backend.delivery.update', compact('delivery'));
  }

  public function update(Request $request, Delivery $delivery) {
    $this->authorize(mapPermission(self::MODULE));
    $delivery->update($this->validateRequest());
    $delivery->storeImage();

    return redirect(route('backend.delivery.index'));
  }

  public function destroy(Delivery $delivery) {
    $this->authorize(mapPermission(self::MODULE));
    $delivery->delete();

    return redirect(route('backend.delivery.index'));
  }

  private function validateRequest() {
    $validatedData = request()->validate([
      "name_th"   => "required",
      "name_en"   => "required",
      "name_cn"   => "required",
      "url"   => "",
      "active" => "",
    ]);

    request()->validate([
      "image"  => ['sometimes', 'file','image','max:2000'],
    ]);

    $validatedData['updated_by'] = Auth::id();
    if(request()->route()->getActionMethod() == 'store') :
      $validatedData['created_by'] = Auth::id();
    endif;

    return $validatedData;
  }

}

