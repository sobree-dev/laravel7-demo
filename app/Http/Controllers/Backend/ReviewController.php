<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Model\Review;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ReviewController extends Controller
{
  const MODULE = 'review';

  public function index(Request $request)
  {
    $this->authorize(mapPermission(self::MODULE));
    if ($request->filled('keyword')):
      $reviews = Review::getDataByKeyword($request->keyword)->orderBy('id', 'desc')->get();
    else:
      $reviews = Review::orderBy('id', 'desc')->limit(50)->get();
    endif;
    return view('backend.review.index', compact('reviews'));
  }

  public function search(Request $request) {
    $this->authorize(mapPermission(self::MODULE));
    if ($request->filled('keyword')):
      $reviews = Review::getDataByKeyword($request->keyword)->orderBy('id', 'desc')->get();
    else:
      $reviews = Review::orderBy('id', 'desc')->all();
    endif;
    return view('backend.review.show', compact('reviews'));
  }

  /**
   * Show the form for creating a new resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function create()
  {
    $this->authorize(mapPermission(self::MODULE));
    $review = new Review;

    return view('backend.review.create', compact('review'));
    
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store(Request $request)
  {
    $this->authorize(mapPermission(self::MODULE));
    $new = Review::create($this->validateRequest());
    $new->storeImage();
    foreach ($request->input('image_detail', []) as $file) {
      $new->addMedia(storage_path('tmp/uploads/' . $file))->toMediaCollection('image_detail');
    }
    deleteImageTmp();
    return redirect(route('backend.review.index'));

  }


  public function edit(Review $review)
  {
    $this->authorize(mapPermission(self::MODULE));
    
    return view('backend.review.update', compact('review')); 
  }

  public function update(Request $request, Review $review)
  {
    $this->authorize(mapPermission(self::MODULE));
    $review->update($this->validateRequest());
    $review->storeImage();

    if (collect($review->image_detail)->count() > 0) {
      foreach ($review->image_detail as $media) {
        if (!in_array($media->file_name, $request->input('image_detail', []))) {
            $media->delete();
        }
      }
    }

    $media = $review->image_detail->pluck('file_name')->toArray();

    foreach ($request->input('image_detail', []) as $file) {
      if (count($media) === 0 || !in_array($file, $media)) {
        $review->addMedia(storage_path('tmp/uploads/' . $file))->toMediaCollection('image_detail');
      }
    }
    deleteImageTmp();
    return redirect(route('backend.review.index'));
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  \App\Model\review  $review
   * @return \Illuminate\Http\Response
   */
  public function destroy(Review $review)
  {
    $this->authorize(mapPermission(self::MODULE));
    $new->delete();
    return redirect(route('backend.review.index'));
  }

  private function validateRequest() {
    
    $validatedData = request()->validate([
      "type"                 => "required",
      "title_th"             => "required",
      "title_en"             => "required",
      "title_cn"             => "required",
      "short_description_th" => "required",
      "short_description_en" => "required",
      "short_description_cn" => "required",
      "description_th"       => "required",
      "description_en"       => "required",
      "description_cn"       => "required",
      "point"                => "required",
      "active"               => "",
    ]);
  
    request()->validate([
      "image_user"      => ['sometimes', 'file','image', 'max:1000'],
      "image"           => ['sometimes', 'file','image', 'max:5000'],
      "upload_detail"    => ['sometimes', 'array'],
      "upload_detail.*"  => ['sometimes', 'file','image', 'max:5000'],
    ]);
   
    $validatedData['updated_by'] = Auth::id();
    if(request()->route()->getActionMethod() == 'store'):
      $validatedData['created_by'] = Auth::id();
    else:
    endif;

    return $validatedData;
  }
}
