<?php
namespace App\Http\Controllers\Backend;
use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
class UserController extends Controller
{
  const MODULE = 'user';

  public function index(Request $request)
  {
    $this->authorize(mapPermission(self::MODULE));
    $search = [];
    if ($request->filled('keyword')) :
      $users = User::getUserByKeyword(request('keyword'))
                      ->ignoreSuperAdmin()
                      ->get(); 
    else:
      $users = User::ignoreSuperAdmin()
                    ->limit(50)
                    ->get();
    endif;

    return view('backend.user.index', compact(['users', 'search']));
  }

  public function search(Request $request) {
    $this->authorize(mapPermission(self::MODULE));
    if ($request->filled('keyword')) :
      $users = User::getUserByKeyword(request('keyword'))
                      ->ignoreSuperAdmin()
                      ->get(); 
    else:
      $users = User::ignoreSuperAdmin()
                    ->limit(50)
                    ->get();
    endif;

    return view('backend.user.show', compact(['users']));
  }

  public function create()
  {
    $user = new User;
    $roles = Role::where('name', '<>', 'super admin')->get();

    return view('backend.user.create', compact(['user', 'roles']));
  }

  public function store(Request $request)
  {
    $this->authorize(mapPermission(self::MODULE));
    $data = request()->validate([
      'first_name' => ['required', 'string', 'max:255'],
      'last_name' => '',
      'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
      'password' => ['required', 'string', 'min:6', 'confirmed'],
      'department_id' => '',
    ]);
    $role_id = request()->validate([ 
      'role_id' => ['required', 'array'],
      'role_id.*' => ['required'],
    ]);
    $data['password'] = Hash::make($data['password']);
    $data['created_by'] = Auth::id();
    $data['updated_by'] = Auth::id(); 
    $user = User::create($data);
    $user->syncRoles(request('role_id'));

    return redirect(route('backend.user.index'));
  }

  public function edit(User $user)
  { 
    $this->authorize(mapPermission(self::MODULE));
    $roles = Role::where('name', '<>', 'super admin')->get();

    return view('backend.user.update', compact(['user', 'roles']));
  }

  public function update(Request $request, User $user)
  {
    $this->authorize(mapPermission(self::MODULE));
    if($request->filled('password')) :
      $data = request()->validate([
              'first_name' => ['required', 'string', 'max:255'],
              'last_name' => '',
              'password' => ['string', 'min:6', 'confirmed'],
              'active' => '',
       ]);
          
      $data['password'] = Hash::make($data['password']);
     else :
      $data = request()->validate([
              'first_name' => ['required', 'string', 'max:255'],
              'last_name' => '',
              'active' => '',
              'department_id' => '',
      ]);
         
    endif;
    $role_id = request()->validate([ 
      'role_id' => ['required', 'array'],
      'role_id.*' => ['required'],
    ]);
   
    $data['updated_by'] = Auth::id();
    $user->update($data);
    $user->syncRoles(request('role_id'));

    return redirect(route('backend.user.index'));
  }

  public function destroy(User $user)
  {
    $this->authorize(mapPermission(self::MODULE));
    $user->delete();
    
    return redirect(route('backend.user.index'));
  }
}
