<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Model\News;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class NewsController extends Controller
{
  const MODULE = 'news';
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
      $this->authorize(mapPermission(self::MODULE));
      if ($request->filled('keyword')) :
        $news = News::getNewsByKeyword($request->keyword)->orderBy('id', 'desc')->get();
      else:
        $news = News::orderBy('id', 'desc')->limit(50)->get();
      endif;
      return view('backend.news.index', compact('news'));
    }

    public function search(Request $request) {
      $this->authorize(mapPermission(self::MODULE));
      if ($request->filled('keyword')) :
        $news = News::getNewsByKeyword($request->keyword)->orderBy('id', 'desc')->get();
      else:
        $news = News::orderBy('id', 'desc')->all();
      endif;
      return view('backend.news.show', compact('news'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
      $this->authorize(mapPermission(self::MODULE));
      $news = new News;

      return view('backend.news.create', compact('news'));
      
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $this->authorize(mapPermission(self::MODULE));
      $new = News::create($this->validateRequest());
      $new->storeImage();
      foreach ($request->input('image_detail', []) as $file) {
        $new->addMedia(storage_path('tmp/uploads/' . $file))->toMediaCollection('image_detail');
      }
      deleteImageTmp();
      return redirect(route('backend.news.index'));

    }


    public function edit(News $news)
    {
      $this->authorize(mapPermission(self::MODULE));
      return view('backend.news.update', compact('news')); 
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Model\News  $news
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, News $news)
    {
      $this->authorize(mapPermission(self::MODULE));
      $news->update($this->validateRequest());
      $news->storeImage();

      if (collect($news->image_detail)->count() > 0) {
        foreach ($news->image_detail as $media) {
          if (!in_array($media->file_name, $request->input('image_detail', []))) {
              $media->delete();
          }
        }
      }

      $media = $news->image_detail->pluck('file_name')->toArray();

      foreach ($request->input('image_detail', []) as $file) {
        if (count($media) === 0 || !in_array($file, $media)) {
          $news->addMedia(storage_path('tmp/uploads/' . $file))->toMediaCollection('image_detail');
        }
      }
      deleteImageTmp();
      return redirect(route('backend.news.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Model\News  $news
     * @return \Illuminate\Http\Response
     */
    public function destroy(News $news)
    {
      $this->authorize(mapPermission(self::MODULE));
      $new->delete();
      return redirect(route('backend.news.index'));
    }

    private function validateRequest() {
      
      $validatedData = request()->validate([
        "title_th"             => "required",
        "title_en"             => "required",
        "short_description_th" => "required",
        "short_description_en" => "required",
        "description_th"       => "required",
        "description_en"       => 'required',
        "active"               => "",
        "type"                 =>"",

      ]);
    
      request()->validate([
          "upload"          => ['sometimes', 'file','image','max:5000'],
          'upload_detail'   => ['sometimes', 'array'],
          'upload_detail.*' => ['sometimes', 'file', 'image', 'max:5000'],
      ]);

     
      $validatedData['updated_by'] = Auth::id();
      if(request()->route()->getActionMethod() == 'store'):
        $validatedData['created_by'] = Auth::id();
      else:
      endif;
     

      return $validatedData;
    }
}
