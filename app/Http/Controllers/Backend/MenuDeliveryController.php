<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\MenuDelivery;
use App\Model\FoodType;
use Illuminate\Support\Facades\Auth;

class MenuDeliveryController extends Controller
{
  const MODULE = 'menu_delivery';

  public function index(Request $request) 
  {
    $this->authorize(mapPermission(self::MODULE));
    if ($request->filled('keyword')):
      $menu_deliveries = MenuDelivery::getDataByKeyword($request->keyword)->get();
    else:
      $menu_deliveries = MenuDelivery::limit(50)->get();
    endif;

    return view('backend.menu_delivery.index', compact('menu_deliveries'));
  }

  public function search(Request $request) {
    $this->authorize(mapPermission(self::MODULE));
    if ($request->filled('keyword')):
      $menu_deliveries = MenuDelivery::getDataByKeyword($request->keyword)->get();
    else:
      $menu_deliveries = MenuDelivery::all();
    endif;

    return view('backend.menu_delivery.show', compact('menu_deliveries'));
  }

  public function create() {
    $this->authorize(mapPermission(self::MODULE));
    $menu_delivery = new MenuDelivery;
    $food_types = FoodType::onlyActive()->get();

    return view('backend.menu_delivery.create', compact('menu_delivery', 'food_types'));

  }

  public function store(Request $request) {
    $this->authorize(mapPermission(self::MODULE));
    $menu_delivery = MenuDelivery::create($this->validateRequest());
    $menu_delivery->storeImage();

    return redirect(route('backend.menu_delivery.index'));
  }

  public function edit(MenuDelivery $menu_delivery) {
    $this->authorize(mapPermission(self::MODULE));
    $food_types = FoodType::onlyActive()->get();

    return view('backend.menu_delivery.update', compact('menu_delivery', 'food_types'));
  }

  public function update(Request $request, MenuDelivery $menu_delivery) {
    $this->authorize(mapPermission(self::MODULE));
    $menu_delivery->update($this->validateRequest());
    $menu_delivery->storeImage();

    return redirect(route('backend.menu_delivery.index'));
  }

  public function destroy(MenuDelivery $menu_delivery) {
    $this->authorize(mapPermission(self::MODULE));
    $menu_delivery->delete();

    return redirect(route('backend.menu_delivery.index'));
  }

  private function validateRequest() {
    $validatedData = request()->validate([
      "name_th"   => "required",
      "name_en"   => "required",
      "name_cn"   => "required",
      "description_th"   => "",
      "description_en"   => "",
      "description_cn"   => "",
      "price"   => "required",
      "detail_th"   => "",
      "detail_en"   => "",
      "detail_cn"   => "",
      "active" => "",
    ]);

    request()->validate([
      "image"  => ['sometimes', 'file','image','max:5000'],
    ]);

    $validatedData['updated_by'] = Auth::id();
    if(request()->route()->getActionMethod() == 'store') :
      $validatedData['created_by'] = Auth::id();
    endif;

    return $validatedData;
  }

}

