<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Facades\App\Repository\Pages;
use Facades\App\Repository\Banners;
use App\Model\MenuAlacarte;
use App\Model\Alacarte;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Illuminate\Support\Str;

class AlacarteMenuController extends Controller
{
  public function index()
  {
    $pages = Pages::get(9);
    $banners = Banners::get(9);
    $recommendeds = MenuAlacarte::onlyActive()->where('recommended', 1)->inRandomOrder()->limit(6)->get();
    $alacarte_menus = MenuAlacarte::onlyActive()->orderBy('food_type_id', 'asc')->get();
    $alacarte = Alacarte::find(1);

    return view('frontend.menus.alacarte', compact(['pages', 'banners', 'recommendeds', 'alacarte_menus', 'alacarte']));
  }

}

