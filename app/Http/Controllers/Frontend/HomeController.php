<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Facades\App\Repository\Pages;
use Facades\App\Repository\Banners;
use App\Model\AboutUs;
use App\Model\Promotion;
use App\Model\MenuAlacarte;
use App\Model\MenuBuffet;
use App\Model\Review;
use App\Model\Delivery;
use App\Model\Branch;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Illuminate\Support\Str;
use Illuminate\Support\Arr;

class HomeController extends Controller
{
  public function index()
  {
    $pages = Pages::get(1);
    $banners = Banners::get(1);
    $aboutus = AboutUs::find(1);
    $promotions = Promotion::onlyActive()->orderBy('end_date', 'desc')->limit(9)->get();
    $alacarte_recommendeds = MenuAlacarte::onlyActive()->where('recommended', 1)->inRandomOrder()->limit(9)->get();
    $buffet_recommendeds = MenuBuffet::onlyActive()->where('recommended', 1)->inRandomOrder()->limit(9)->get();
    $merged = $alacarte_recommendeds->merge($buffet_recommendeds);
    $recommendeds = Arr::shuffle($merged->all());
    $main_review = Banners::get(5);
    $reviews = Review::onlyActive()->where('type', '<>', 'customer')->orderBy('id', 'desc')->limit(9)->get();
    $customer_reviews = Review::onlyActive()->where('type', 'customer')->orderBy('id', 'desc')->limit(9)->get();
    $deliveries = Delivery::onlyActive()->inRandomOrder()->limit(3)->get();
    $branchs = Branch::onlyActive()->inRandomOrder()->limit(3)->get();

    return view('frontend.home.index', compact(['pages', 'banners', 'aboutus', 'promotions', 'recommendeds', 'main_review', 'reviews', 'customer_reviews', 'deliveries', 'branchs']));
  }

  public function searchPage(Request $request)
  {
    $pages = Pages::get(1);
    $search = $request->q;
    return view('frontend.home.search-page', compact(['pages', 'search']));
  }

}

