<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Facades\App\Repository\Pages;
use Facades\App\Repository\Banners;
use App\Model\Promotion;
use App\Model\ViewActivityLog;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Illuminate\Support\Str;

class PromotionController extends Controller
{
  CONST MODEL = 'Promotion';
  public function index()
  {
    $pages = Pages::get(3);
    $banners = Banners::get(3);
    $promotions = Promotion::onlyActive()->whereRaw('curdate() between start_date and end_date')->orderBy('start_date', 'desc')->paginate(12);

    return view('frontend.promotion.index', compact(['pages', 'banners', 'promotions']));
  }

  public function detail($locate, Promotion $promotion, Request $request)
  {
    add_log(self::MODEL, $promotion->id);
    $pages = Pages::get(3);
    $banners = Banners::get(3);
    $views = get_log_action(self::MODEL, $promotion->id);
    $shares = get_log_action(self::MODEL, $promotion->id, 'share');

    return view('frontend.promotion.detail', compact(['pages', 'banners', 'promotion', 'views', 'shares']));
  }

}

