<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Facades\App\Repository\Pages;
use Facades\App\Repository\Banners;
use App\Model\Review;
use App\Model\ViewActivityLog;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Illuminate\Support\Str;

class ReviewController extends Controller
{
  CONST MODEL = 'Review';
  public function index()
  {
    $pages = Pages::get(5);
    $banners = Banners::get(5);
    $reviews = Review::onlyActive()->where('type', '<>', 'customer')->orderBy('id', 'desc')->paginate(12);

    return view('frontend.review.index', compact(['pages', 'banners', 'reviews']));
  }

  public function detail($locate, Review $review, Request $request)
  {
    add_log(self::MODEL, $review->id);
    $pages = Pages::get(5);
    $banners = Banners::get(5);
    $views = get_log_action(self::MODEL, $review->id);
    $shares = get_log_action(self::MODEL, $review->id, 'share');

    return view('frontend.review.detail', compact(['pages', 'banners', 'review', 'views', 'shares']));
  }

}

