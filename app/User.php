<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Spatie\Permission\Traits\HasRoles;
use Spatie\Activitylog\Traits\LogsActivity;
use Illuminate\Database\Eloquent\SoftDeletes;

class User extends Authenticatable
{
    use Notifiable, HasRoles, LogsActivity, SoftDeletes;

    protected static $logName = 'user';
    protected static $logAttributes = ['*'];
    protected static $logOnlyDirty = true;
  
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'created_by', 'updated_by', 'first_name', 'last_name'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function user_updates() {
      return $this->belongsTo('App\User', 'updated_by');
    }

    public function scopegetUserByKeyword($query, $keyword) {
      return $query->where('first_name', 'like', "%$keyword%");
    }

    public function scopeignoreSuperAdmin($query) {
       return $query->where('first_name', 'not like', "%SuperAdmin%");
    }

     public function getNameAttribute() {
        return "{$this->first_name} {$this->last_name}";
    }

    /*public function getActiveAttribute($attributes) {
        return  [ 
                   1 => 'Active' ,
                   0 =>'Inactive'
                ][$attributes];
    }*/

      public function isActive()
    {
        return $this->active === 1;
    }




}
