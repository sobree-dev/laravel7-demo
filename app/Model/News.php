<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Spatie\MediaLibrary\Models\Media;

class News extends Model implements HasMedia
{
  use LogsActivity, HasMediaTrait;
  protected $table = 'news';
  protected $guarded = [];

  protected static $logName = 'news';
  protected static $logAttributes = ['*'];
  protected static $logOnlyDirty = true;
  protected $attributes = [
        'active' => 1,
    ];

  public function registerMediaCollections()
  {
    $this->addMediaCollection('image')->singleFile();
  }

  public function storeImage()
  {
    if (request()->has('image')) {
      $this->addMediaFromRequest('image')
          ->sanitizingFileName(function($fileName) {
            return sanitizeFileName($fileName);
           })->toMediaCollection('image');
    }
  }

   public function getImageAttribute()
  {
    return $this->getFirstMediaUrl('image');
  }

   public function getImageDetailAttribute()
  {
    return $this->getMedia('image_detail');
  }

  public function getActiveAttribute($attributes) {
    return  [ 
               1 => 'Active' ,
               0 =>'Inactive'
            ][$attributes];
  }

  public function update_name() {
    return $this->hasOne('App\User', 'id', 'updated_by');
  }

  public function scopegetNewsByKeyword($query, $keyword) {
    return $query->where('title_th', 'like', "%$keyword%")
                ->orWhere('title_en', 'like', "%$keyword%"); 
  }

   public function scopeonlyActive($query) {
    return $query->where('active', 1);
  }

}
