<?php
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Cache;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', function () {
  return redirect(route('frontend.home', ['locale' => app()->getLocale()]));
});

Route::name('frontend.')
  ->namespace('Frontend')
  ->prefix('{locale}') 
  ->where(['locale' => '[a-zA-Z]{2}']) 
  ->middleware('setlocale')
  ->group(function(){

    Route::get('/home', 'HomeController@index')->name('home');
    Route::get('/about', 'AboutController@index')->name('about');
    Route::get('/promotion', 'PromotionController@index')->name('promotion');
    Route::get('/promotion/{promotion}', 'PromotionController@detail')->name('promotion-detail');
    Route::get('/review', 'ReviewController@index')->name('review');
    Route::get('/review/{review}', 'ReviewController@detail')->name('review-detail');
    Route::get('/gallery', 'GalleryController@index')->name('gallery');
    Route::get('/branch', 'BranchController@index')->name('branch');
    Route::get('/contactus', 'ContactUsController@index')->name('contact');
    Route::get('/menu_alacarte', 'AlacarteMenuController@index')->name('alacarte_menu');
    Route::get('/menu_buffet', 'BuffetMenuController@index')->name('buffet_menu');
    Route::get('/menu_delivery', 'DeliveryMenuController@index')->name('delivery_menu');

    Route::resource('/contact', 'ContactUsController');

    Route::get('/lang', function () {
      return back()->withInput(['locale' => app()->getLocale()]);
      //return redirect(route('frontend.home', ['locale' => app()->getLocale()]));
    });

    /* Sitemap Route*/
    Route::get('/sitemap.xml', 'SitemapController@index')->name('sitemap.xml');
    Route::get('/sitemap.xml/reviews', 'SitemapController@reviews');
});

Route::name('frontend.')
->namespace('Frontend')
  ->group(function(){

    set_time_limit(0);
    
    Route::get('/clearCache', function () {
      Cache::flush();
      $exitCode = Artisan::call('config:cache');
      return 'Cache Clear';
    });

    Route::get('/clearConfig', function () {
      $exitCode = Artisan::call('config:clear');
      return 'Config Clear';
    });

    Route::get('/createKey', function () {
      Artisan::call('key:generate');
      return 'Success';
    });

    Route::get('/createLink', function () {
      Artisan::call('storage:link');
      return 'Success';
    });

    Route::get('/migrate', function () {
      Artisan::call('migrate:fresh --seed');
      return 'Success';
    });

});


Auth::routes();

